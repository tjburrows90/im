
__metaclass__ = type

import numpy as np
import operator
import os
import inspect
from .common import Storage

#-------------------------------------------------------------------------
# methods for operator and numpy support for IM subclass objects


class npMethods(object):
    """ Class to contain support for math type operations on IM subclass objects.
    Separated from the IM object to keep syntax tidy.
    There are two main types of inheritable methods here.
    _operation and _operation_np
    _operator
    """

    _sep = '='  # separator between arguments for the name

    def _make_name(self, operation, other_str):
        return '{0}{1}{2}'.format(operation.__name__, self._sep, other_str)

#-------------------------------------------------------------------------

    def _operation(self, other, operation, resultIsNewObj=True, asArray=False):
        """
        Perform operation on self and other as in operation(self,other).
        Method disigned to return a new object based on the method
            standard operators eg. + - * ** / %

        if resutIsNewObj: A new object with same type as self is returned with
            the result. The new object will be alwasy be a new object and
            autonamed based on the operation.
        else: The result is applied to this object in place which is then
        returned.


        Parameters:
        ----------
        other: obj
            if other is self and the operation only takes one argument then
            other may be passed as self

        resultIsNewObj: Bool
                Will return the same obect if True, otherwise will perform
                operation in this object.
        asArray: Bool
            if asArray then the object returned will be an array of the result

        Returns:
        -------
        obj: type(self) | ndarray
        An obect of the same type as self.

        Notes:
            see the help for the 'operator' module for more information
        """
        def finish():
            """common code to add notes and return the object
            """
            imn.notes = 'Result of operation:\n\nfor\n{0}\n{1}\n{2}'.format(
                repr(self), operation.__name__, repr(other))

            if not asArray:
                return imn
            else:
                return np.array(imn.axes)

        # functionality if same type (or if derived from one another)
        if isinstance(other, self._objects['__IM__']) and len(
                self.axes) == len(other.axes):

            # if there are a different number of axes
            if len(self.axes) != len(other.axes):
                raise ArithmeticError(
                    'Objects not compatible (different number of axes)')
            if (self.shape[1:] == other.shape[1:]):
                if resultIsNewObj:
                    name = self._make_name(operation,
                                           other.name.replace(os.sep, '___'))
                    imn = self._new_im(self.buffer.image_sub_type, name=name,
                                       inherit_variables=True, force_new=True)
                    imn.parents = self
                    imn.parents = other
                else:
                    imn = self

                for a, b, c in zip(imn.axes, self.axes, other.axes):
                    if np.isnan(b).any() or np.isnan(c).any():
                        raise ValueError(
                            'Data contains "nan" values: {0}'.format(self))
                    try:
                        if b.shape[0] == b.shape[0]:
                            a[:] = operation(b, c)
                        elif c.shape[0] == 1:
                            for i in range(len(a)):
                                a[i] = operation(b[i], c[0])
                        else:
                            raise IndexError("Shape missmatch. Operation not"
                                             " possible for objects of shape {0} and {1}".format(b.shape, c.shape))

                    except TypeError:
                        if other is self:
                            a[:] = operation(b)
                        else:
                            raise

                return finish()

            else:
                # The shape in this context refers to each axes
                msg = 'Cannot compute operator.{0}'\
                    ' because shapes are different\n{1}\n{2}'.format(
                        operation.__name__, repr(self), repr(other))
                raise ArithmeticError(msg)

        elif isinstance(other, Storage):
            # another type:
            msg = 'This type of interclass operation has not  been developed '\
                'yet! {0} on {1} (not really a bug, just needs development...)'

            raise ArithmeticError(msg.format(other.__class__, self.__class__))

        elif not isinstance(other, np.ndarray):
            other = np.array(other)

        if other.size == 1:
            if float(str(other)).is_integer():
                val = int(other)
            else:
                val = 'const'

            if resultIsNewObj:
                name = self._make_name(operation, val)
                imn = self._new_im(self.buffer.image_sub_type, name=name,
                                   inherit_variables=True, force_new=True)
            else:
                imn = self
            for a, b in zip(imn.axes, self.axes):
                a[:] = operation(b, other)
            return finish()

        if other.shape == 3:
            if resultIsNewObj:
                name = self._make_name(operation, '3darray')
                imn = self._new_im(self.buffer.image_sub_type, name=name,
                                   inherit_variables=True, force_new=True)
            else:
                imn = self
            for a, b in zip(imn.axes, self.axes):
                a[:] = operation(b, other)
            return finish()

        if len(other.shape) == 2:
            if resultIsNewObj:
                name = self._make_name(operation, '2darray')
                imn = self._new_im(self.buffer.image_sub_type, name=name,
                                   inherit_variables=True, force_new=True)
            else:
                imn = self

            if (self.shape[1] == other.shape[0]) and (
                    self.shape[2] == other.shape[1]):
                for a, b in zip(imn.axes, self.axes):
                    for c, d in zip(a, b):
                        c[:] = operation(d, other)
            # Other is a 2 dimensional array but the first dimension == number
            # of axes
            elif (other.shape[0] == len(self.axes)) and (self.shape[0] == other.shape[1]):
                for a, b in zip(imn.axes, self.axes):
                    for i in range(other.shape[1]):
                        a[i] = operation(b[i], other[0][i])

            # Other is a 2 dimensional array but the second dimension == number
            # of axes
            elif (other.shape[1] == len(self.axes)) and (self.shape[0] == other.shape[0]):
                for a, b in zip(imn.axes, self.axes):
                    for i in range(other.shape[0]):
                        a[i] = operation(b[i], other[i])
            else:
                raise ValueError("No operation possible for array of shape {0} and {0}".format(
                    self.shape, other.shape))

            return finish()

        # Other is an array with the same number of elements as there are
        # frames
        elif (len(other.shape) == 1) and (other.shape[0] == self.shape[0]):
            if resultIsNewObj:
                name = self._make_name(operation, '2darray')
                imn = self._new_im(self.buffer.image_sub_type, name=name,
                                   inherit_variables=True, force_new=True)
            else:
                imn = self
            for a, b in zip(imn.axes, self.axes):
                for i in range(other.shape[0]):
                    a[i] = operation(b[i], other[i])
            return finish()

        else:
            raise TypeError('Operation not permitted with object: {0}'.format(
                other))

    def __hash__(self):
        # required because __eq__ is defined
        return id(self)


#-------------------------------------------------------------------------
# DEFINE THE METHODS BELOW (operator type)
    # operations which result in a new object being created

    def __lt__(self, other):
        operation = getattr(operator, '__lt__')
        return self._operation(other, operation, True, True)

    def __le__(self, other):
        operation = getattr(operator, '__le__')
        return self._operation(other, operation, True, True)

    def __eq__(self, other):
        operation = getattr(operator, '__eq__')
        return self._operation(other, operation, True, True)

    def __ne__(self, other):
        operation = getattr(operator, '__ne__')
        return self._operation(other, operation, True, True)

    def __ge__(self, other):
        operation = getattr(operator, '__ge__')
        return self._operation(other, operation, True, True)

    def __gt__(self, other):
        operation = getattr(operator, '__gt__')
        return self._operation(other, operation, True, True)

    def __abs__(self):
        operation = getattr(operator, '__abs__')
        return self._operation(self, operation)

    def __add__(self, other):
        operation = getattr(operator, '__add__')
        return self._operation(other, operation)

    def __div__(self, other):
        operation = getattr(operator, '__div__')
        return self._operation(other, operation)

    def __floordiv__(self, other):
        operation = getattr(operator, '__floordiv__')
        return self._operation(other, operation)

    def __invert__(self):
        operation = getattr(operator, '__invert__')
        return self._operation(self, operation)

    def __lshift__(self, other):
        operation = getattr(operator, '__lshift__')
        return self._operation(other, operation)

    def __mod__(self, other):
        operation = getattr(operator, '__mod__')
        return self._operation(other, operation)

    def __mul__(self, other):
        operation = getattr(operator, '__mul__')
        return self._operation(other, operation)

    def __neg__(self):
        operation = getattr(operator, '__neg__')
        return self._operation(self, operation)

    def __pow__(self, other):
        operation = getattr(operator, '__pow__')
        return self._operation(other, operation)

    def __rshift__(self, other):
        operation = getattr(operator, '__rshift__')
        return self._operation(other, operation)

    def __sub__(self, other):
        operation = getattr(operator, '__sub__')
        return self._operation(other, operation)

    def __truediv__(self, other):
        operation = getattr(operator, '__truediv__')
        return self._operation(other, operation)

    def __xor__(self, other):
        operation = getattr(operator, '__xor__')
        return self._operation(other, operation)

    def __concat__(self, other):
        operation = getattr(operator, '__concat__')
        return self._operation(other, operation)

    def _ilshift__(self, other):
        operation = getattr(operator, '_ilshift__')
        return self._operation(other, operation, False)

    def _iconcat__(self, other):
        operation = getattr(operator, '_iconcat__')
        return self._operation(other, operation, False)

    def __iadd__(self, other):
        operation = getattr(operator, '__iadd__')
        return self._operation(other, operation, False)

    def __isub__(self, other):
        operation = getattr(operator, '__isub__')
        return self._operation(other, operation, False)

    def __itruediv__(self, other):
        operation = getattr(operator, '__itruediv__')
        return self._operation(other, operation, False)

    def _ior__(self, other):
        operation = getattr(operator, '_ior__')
        return self._operation(other, operation, False)

    def _ipow__(self, other):
        operation = getattr(operator, '_ipow__')
        return self._operation(other, operation, False)

    def _imod__(self, other):
        operation = getattr(operator, '_imod__')
        return self._operation(other, operation, False)

    def _imul__(self, other):
        operation = getattr(operator, '_imul__')
        return self._operation(other, operation, False)

#-------------------------------------------------------------------------
# numpy type operations

    def _operation_np(self, operation, axis, out=None, **kwargs):
        """
        Returns a new object with np."operation" performed on object the object.

        Parameters
        ----------
        operation: func
            Any function which accepts an n dimensional array such as np.sum
        axis: None | integer
            The axis on which to perform the operation corresponding to normal
            usage of 'axis' in the 'operation.
        out: ndarray
            If provided, the output array.
        kwargs: dict
            Named arguments will be passed to operation when called.

        Returns
        -------
        Depends on axis: as follows
            axis == 0      returns same object type as this method
            axis == None,>=1   returns ndarray of result from operation with shape

        Notes:
        If axis == 0: an object of the same type with an appropriate shape is returned
        If axis is None, or >= 1 then an array of suitable shape based on the number of axes
        in the object is returned
        """
        if out is not None:
            kwargs.append(out)
        if axis is None or axis > 0:
            output = []
            for a in self.axes:
                output.append(operation(a, axis, **kwargs))
            return np.array(output)

        if axis == 0:
            name = self._make_name(operation, 'numpy')
##            im = self._new_im(self.buffer.image_sub_type, None, name=name, force_new=True)
            im = self.copy(name=name, force_new=True, frames=1)

            try:
                for a, b in zip(im.axes, self.axes):
                    a[:] = operation(b, axis=axis, **kwargs)
            except TypeError:
                import sys
                # sometimes axis is not permitted. however it should be passed
                # as zero
                if str(sys.exc_info()[1]).find("'axis'") == 0 and axis == 0:
                    try:
                        for a, b in zip(im.axes, self.axes):
                            a[:] = operation(b, **kwargs)
                    except ValueError:
                        im = self.copy(name=name, force_new=True)
                        for a, b in zip(im.axes, self.axes):
                            a[:] = operation(b, **kwargs)
                else:
                    raise

            im.notes = 'Set values as result of:\n%sof:\n%s\nacross all frames' % (
                operation, self)
            return im

#-------------------------------------------------------------------------
# DEFINE THE METHODS BELOW (numpy type)

    def mean(self, axis=0, dtype=None, out=None):
        return self._operation_np(np.mean, axis, dtype=dtype, out=out)

    def max(self, axis=0, dtype=None, out=None):
        return self._operation_np(np.max, axis, out=out)

    def min(self, axis=0, out=None):
        return self._operation_np(np.min, axis, out=out)

    def abs(self, axis=0, out=None):
        """ Absolute value not the magnitude of a vector. See mag for magnitude.
        """
        return self._operation_np(np.abs, axis, out=out)

    def sum(self, axis=0, out=None):
        return self._operation_np(np.sum, axis, out=out)

    def all(self, axis=None, out=None):
        res = self._operation_np(np.all, axis, out=out)
        return res.all()

    def any(self, axis=None, out=None):
        res = self._operation_np(np.all, axis, out=out)
        return res.any()
