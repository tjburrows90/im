from __future__ import division, absolute_import

"""A collection of functions and classes expected to have multiple purposes
the Storage() class is capable of loading and reloading data using pickles only
"""
__metaclass__ = type

import shutil
import os
import re
import pickle
import sys
from time import ctime
from weakref import WeakValueDictionary
import weakref
from collections import OrderedDict
from configobj import ConfigObj as ConfigObjCls
import configobj
import ast
import warnings
import time
import glob
import copy
from random import random
from functools import wraps
import io
import ReadIM
import tempfile
import psutil
import numpy as np
from ReadIM.extra import BunchMappable
npNamespace = {'array': np.array}


# try to locate ImageMagick (otherwise this must be specified later)
IMAGICK_EXEC = ""
for p in os.environ['PATH'].split(';'):
    p = glob.glob(os.path.join(p, 'convert.exe'))
    if p:
        for f in p:
            if f.find('ImageMagick') > 0:
                IMAGICK_EXEC = f
                break
            else:
                continue
    if IMAGICK_EXEC:
        break

if sys.version_info < (3, 0):
    STR_TYPE = basestring
else:
    STR_TYPE = str

__all__ = ['copy_file_passive', 'make_dir_passive', 'make_pickle', 'load_pickled',
           'iterflatten', 'get_name_atts', 'Bunch', 'BunchMappable',
           'Storage', 'calc_once']

# Globals
# -----------------------------------------------------------------------------
PRINT_LOADSAVE = False
PRINT_DEFAULTS_MESSAGE = False
PRINT_CONFIG_MESSAGES = False
RAISE_CONFIG_ERRORS = False
LOAD_ATTEMPTS_READ = 20
LOAD_ATTEMPTS_WRITE = 20
DEBUG = False
CFG_MAIN_DEFAULTS = {}
TYPE_SEP = '.'
DEAFAULT_LIST_SEP = ';'

WRITE_MODE = ['pickle', 'hickle'][0]

# if WRITE_MODE == 'hickle':

def hickle_file_opener(f, mode='r', track_times=True):
    """ A file opener helper function with some error handling.  This can open
    files through a file object, a h5py file, or just the filename.

    Args:
        f (file, h5py.File, or string): File-identifier, e.g. filename or file object.
        mode (str): File open mode. Only required if opening by filename string.
        track_times (bool): Track time in HDF5; turn off if you want hickling at
                 different times to produce identical files (e.g. for MD5 hash check).

    """
    # Where we handed a file object or just a file name string?
    if isinstance(f, file) or hasattr(f, 'close'):
        filename, mode = f.name, f.mode
        f.close()
        h5f = h5.File(filename, 'w')
    elif isinstance(f, str) or isinstance(f, unicode):
        filename = f
        h5f = h5.File(filename, mode)
    elif isinstance(f, H5FileWrapper) or isinstance(f, h5._hl.files.File):
        try:
            filename = f.filename
        except ValueError:
            raise ClosedFileError()
        h5f = f
    else:
        print(type(f))
        raise FileError

    h5f.__class__ = H5FileWrapper
    h5f.track_times = track_times
    return h5f
try:
    import hickle
    import h5py as h5
    from hickle import H5FileWrapper

# overload hickle_file_opener so we can use a SpooledTemporaryFile
#-------------------------------------------------------------------------


    hickle.file_opener = hickle_file_opener
except ImportError:
    import warnings
    msg = 'Unable to import hickle, reverting to pickle.'

    warnings.warn(msg, ImportWarning)
    WRITE_MODE = 'pickle'

# wrap config object which can switch to default encoding


def ConfigObj(*args, **kwargs):

    try:
        return ConfigObjCls(*args, **kwargs)
    except BaseException:
        if 'encoding' not in kwargs:
            return ConfigObj(*args, encoding='latin1', **kwargs)
        raise


def _get_extension(WRITE_MODE):
    "return the extension based on the write mode"
    return {'hickle': '.h5',
            'pickle': '.pkl'}[WRITE_MODE]

# Moving a Class
# -----------------------------------------------------------------------------


def copy_file_passive(src, dst):
    """ Checks if the file already exists first
    """
    if not os.path.isfile(dst):
        make_dir_passive(os.path.split(dst)[0])
        shutil.copy(src, dst)


def make_dir_passive(dst):
    """ Checks if the directory already exists before creating
    """
    try:
        if not os.path.isdir(dst):
            os.makedirs(dst)
    except WindowsError:
        pass
    return os.path.abspath(dst)


def make_pickle(obj, filename, protocol=2):
    """ cPickle an object, creating folders as required
    """
    assert isinstance(filename, str)

    folder = os.path.dirname(filename)
    if not os.path.isdir(folder):
        os.makedirs(folder)

    for i in range(LOAD_ATTEMPTS_WRITE):
        try:
            with io.open(filename, 'wb') as output:
                # dump the output. But protect against external exceptions
                pickle.dump(obj, output, protocol)
                return

        except EOFError:
            wait = 0.1 * 10**(random())
            warnings.warn(
                'problem opening file attempt: {0} waiting: {1:0.2f}s'.format(i + 1, wait))
            time.sleep(wait)
            if i is LOAD_ATTEMPTS_WRITE - 2 and os.path.isfile(filename):
                os.remove(filename)
    raise



# overwrite hickle file opener to accept tempfile
#-------------------------------------------------------------------------



#-------------------------------------------------------------------------


def make_hickle(data, filename, compression='gzip', **kwargs):
    """ hickle an object, creating folders as required
    """

    assert isinstance(filename, str)

    folder = os.path.dirname(filename)
    if not os.path.isdir(folder):
        os.makedirs(folder)
    if not filename[-3:].lower() == '.h5':
        filename = filename + '.h5'

    # hickle fails on empty containers. Remove empty containers at the first level.
    # does not currently check for empty containers at deeper levels.

    if isinstance(data, dict):
        out = {}
        for k in data:
            if k == 'buffer':
                out[k] = str(ReadIM.BufferTypeAlt(data[k]).__dict__)
# elif k is 'notes':
##                out[k] = str(data[k])
            else:
                if isIterable(data[k]):
                    try:
                        if not any(data[k]):
                            continue
                    except ValueError:
                        # arrays are valid, but will raise a value error. Let
                        # the slip through here.
                        pass
                elif not data[k]:
                    continue
                out[k] = data[k]
    else:
        out = data
    if os.path.isfile(filename):
        os.remove(filename)
    max_size = psutil.swap_memory()[2] * 3 / 4
    try:
        with tempfile.SpooledTemporaryFile(max_size) as f:
            f.name = 'myTempfileCompressed.h5'
            hickle.dump(out, f, compression=compression, **kwargs)
            os.rename(f.name, filename)
    except BaseException:
        raise
        with tempfile.SpooledTemporaryFile(max_size) as f:
            f.name = 'myTempfile.h5'
            hickle.dump(data, f, compression=None, **kwargs)
            os.rename(f, filename)
            import warnings
            warnings.warn("File stored as h5 with no compression. Probably"
                          "because one of the data items does not support compression{0}".format(filename))


def write_file(data, filename, mode, **kwargs):
    """
    mode should be either pickle or hickle
    """
    writer = {'pickle': make_pickle,
              'hickle': make_hickle}[mode]
    try:
        writer(data, filename, **kwargs)
    except BaseException:
        raise


def load_pickled(filename):
    """return an object from a pickled file"""

    for i in range(LOAD_ATTEMPTS_READ):
        try:
            try:
                with io.open(filename, 'rb') as pklfile:
                    return pickle.load(pklfile)
            except UnicodeDecodeError:
                with io.open(filename, 'rb') as pklfile:
                    return pickle.load(pklfile, encoding='latin1')

        except EOFError:
            wait = 0.1 * 10**(random())
            warnings.warn(
                'problem opening file attempt: {0} waiting: {1:0.2f}s'.format(i + 1, wait))
            time.sleep(wait)
    raise


def load_hickle(filename):
    """return an object from a hickle (hdf5) file """
    try:
        data = hickle.load(filename)
        # a hack for the buffer objects stored as dictionaries to enable
        # compression
        if 'buffer' in data and isinstance(data['buffer'], str):
            data['buffer'] = eval(data['buffer'], npNamespace)
# if 'notes' in data and isinstance(data['notes'], str):
##            data['notes'] = eval(data['notes'])
    except IOError:
        raise IOError('unable to load file {0}'.format(filename))
    return data


def read_file(filename):
    """
    """
    extn = os.path.splitext(filename)[1]

    loader = {'.pkl': load_pickled,
              '.h5': load_hickle,
              '.vector': load_pickled,
              '.image': load_pickled}[extn]

    return loader(filename)


def isIterable(iterable):
    """Test for iterable-ness."""
# source: http://aspn.activestate.com/ASPN/Mail/Message/python-tutor/2302348
    try:
        iter(iterable)
    except TypeError:
        return False
    return True


def isBadIterable(iterable):
    """Return True if it's a 'bad' iterable.

    Note: string's are bad because, when iterated they return
strings 		    making itterflatten loop infinitely.
    """

    if isinstance(iterable, STR_TYPE):
        return True
    if isinstance(iterable, Storage):
        return True

    return False


def iterflatten(iterable):
    """Return a flattened iterator."""
    if isBadIterable(iterable):
        yield iterable
    else:
        try:
            it = iter(iterable)
            for e in it:
                if isIterable(e) and not isBadIterable(e):
                    # Recurse into iterators.
                    for f in iterflatten(e):
                        yield f
                else:
                    if isBadIterable(iterable):
                        yield iterable
                        break
                    else:
                        yield e
        except TypeError:
            yield iterable


def to_gif_func(functions, frames, dst, fignum=None, delay=50,
                extn='png', clf=True, savefigargs={}):
    """
    for all funct in functions:
        calls function(frame, fig)
    delay is in deci-seconds
    clf = True will clear the figure between each frame

    REQUIRES IMAGEMAGICK to be installed

    """

    #
    while not IMAGICK_EXEC:
        path = eval(input('Path to ImageMagick executable'))
        if os.path.isfile(path):
            if path.find('convert.exe') > 0:
                IMAGICK_EXEC[0] = path
            else:
                print('Incorrect executable! path must be to the file "convert.exe"')
        else:
            print('Not a file!')

    import webbrowser
    import subprocess
    import shutil
    import tempfile
    import time
    from matplotlib import pyplot as plt

    CD = os.getcwd()
    tdir = tempfile.mkdtemp()
    os.chdir(tdir)

    if dst is None:
        dst = os.path.join(tdir, 'temp.gif')
    else:
        dst = os.path.abspath(dst)
    if not re.search('.gif', dst, flags=re.IGNORECASE):
        dst += '.gif'

    fig = plt.figure(fignum)
    fignum = fig.number

    try:
        for f in frames:
            if clf:
                fig.clf()

            for func in iterflatten(functions):
                func(f, fignum)

            figname = '%06d.%s' % (f, extn)
            fig.savefig(figname, **savefigargs)

        wcard = '*.%s' % extn

    # create the gif
        outfile = "outfile.gif"
        # make sure you use the static version of the IMAGICK_EXEC this will
        # fail otherwise!!!!
        p = subprocess.call(
            [IMAGICK_EXEC, '-delay', str(delay), wcard, outfile])

        if not p:
            shutil.move(outfile, dst)
            print('Created gif\n%s' % dst)

            webbrowser.open('file://%s' % dst)
            time.sleep(1)
        else:
            print('unable to create gif, error code = %s' % p)
    # clean up
    finally:
        os.chdir(CD)
        try:
            shutil.rmtree(tdir)
        except BaseException:
            pass
    return fig


def get_name_atts(filename, strip_extn=True):
    """
    extract attributes from a filename where attributes are separated by underscores
    eg. H=0.07m
    """
    if strip_extn:
        name = os.path.splitext(filename)[0]
    else:
        name = filename
    candidates = [p for p in name.split(os.sep) if p.find('=') > 0]
    atts = OrderedDict()
    for c in candidates:
        for cc in c.split('_'):
            if cc.find('='):
                kv = cc.split('=')
                if len(kv) == 2:
                    atts[kv[0]] = kv[1]
    return atts


def str2list(st, sep=None):
    """ Covert a string to a list of strings based on the separator
    """
    if sep is None:
        sep = DEAFAULT_LIST_SEP
    return [ch.strip() for ch in st.split(sep) if ch]


def str2dict(st, sepdict='='):
    """ Covert a string to a dictionary
    """
    out = {}
    try:
        for s in st.split(DEAFAULT_LIST_SEP):
            key, val = s.split(sepdict)
            out[key.strip()] = str2num(val.strip())
    except BaseException:
        return st

    return out


def str2num(s, sep=None, sepdict='='):
    """ convert a string to a number. Also considers lists!
    """
    if sep is None:
        sep = DEAFAULT_LIST_SEP
    if isinstance(s, STR_TYPE):
        if s.isdigit():
            return int(s)
        try:
            return ast.literal_eval(s)
        except BaseException:
            if s.find(sep) > 0:
                return str2num(str2list(s, sep), sep, sepdict)
            elif s.find(sepdict) > 0:
                return str2dict(s, sepdict)
            else:
                return s

    elif isIterable(s):
        try:
            # mappable
            out = {}
            for ss in s:
                out[ss] = str2num(s[ss])
            return out
        except TypeError:
            # lists
            return [str2num(ss) for ss in s]
    else:
        return s


def make_config_name(cls, n):
    return '_'.join(
        (cls.__name__, (os.path.basename(n)).replace('.ini', '') + '.ini'))


def _get_family_from_repr(rep):
    return re.search('family="(.*?)"', rep).groups()[0]


def evalAttributes(obj, data, ):
    """ Given an object and data. This function will evaluate any string expression
    which starts with <eval> and will return the evaluated object. In the case that
    data is a list or dictionary, the corresponding entry will be replaced by the
    evaluated expression.
    in data space of obj.
    example:
        <eval>getattr(obj, __class__).__name__
    """
    _magic = '<eval>'

    def hasmagic(d): return str(d).find(_magic) >= 0

    if not hasmagic(data):
        return data

    # strings
    if isinstance(data, STR_TYPE):
        # this is where we evaluate the magic
        if data.find(_magic) == 0:
            return eval(data[len(_magic):])
        return data

    # dictionary
    if hasattr(data, 'keys'):
        for k in list(data.keys()):
            if hasmagic(data[k]):
                data[k] = evalAttributes(obj, data[k])
    # list
    for i, d in enumerate(data):
        if hasmagic(d):
            data[i] = evalAttributes(obj, d)

    return data


# decorators
# -----------------------------------------------------------------------------


def __storage_new__(__new__):
    """ This decortator performs then necessary actions for creating a storage instance.
        Logic in here is used for naming the object for retrieval at a later time.

        This decorator should be applied to any subclasses of Storage which use
        the __new__ method.

        Custom naming of objects should be performed by over writing the staticmethod:
            "_getfullname_and_root"

    for example:

    overriding the new subclass with the following method has modified the name
    to include details of a parent also

    @staticmethod
    def _getfullname_and_root(cls, name, usenameargs=True, **kwargs):

        if not os.path.isabs(name):

            parent  = kwargs.get('parent')
            if parent:
                if isinstance(parent, Storage):
                    parent = parent.name
            else:
                parent = kwargs['family']

            name    = '_'.join([parent, str(name)])

        return super(Group, cls)._getfullname_and_root(cls, name, usenameargs, **kwargs)

    """
    @wraps(__new__)
    def echo_func_storage__new__(cls, name, *args, **kwargs):

        inst = None

        if isinstance(name, cls):
            inst = name

        else:

            # _LOADINFO is a switch so this section is only run once per instantiation
            # -----------------------------------------------------------------------------

            if not '_LOADINFO' in kwargs:

                # -----------------------------------------------------------------------------
                family = kwargs.get('family')
                if not family:
                    fams = Storage._families

                    if len(fams) == 0:
                        family = cls._cfgmainkey  # default family

                    elif len(fams) == 1:
                        family = list(fams)[0]
                    else:
                        raise RuntimeError(
                            'Too many families to automatically assign. Use the keyword "family"'.format(cls))

                if isinstance(family, Storage):
                    family = family.name
                kwargs['family'] = str(family)
# -----------------------------------------------------------------------------

                if not isinstance(name, Storage):
                    if not isinstance(name, STR_TYPE):
                        mesg = "name must be either Storage or str not {0}".format(
                            type(name))
                        raise TypeError(mesg)
                    name = str(name)

                if isinstance(name, STR_TYPE):
                    # this should be a representation

                    if os.path.abspath(name) == name:

                        kwargs['usenameargs'] = False

                fullname, root, rep = cls.get_fullname_root_and_rep(
                    cls, name, **kwargs)
                kwargs['_LOADINFO'] = [fullname, rep, root]

# -----------------------------------------------------------------------------
# end _LOADINFO (run once)

            if '_LOADINFO' in kwargs:
                fullname, rep, root = kwargs['_LOADINFO']

                inst = None

                # object is already alive
                if rep in cls._instances:
                    if kwargs.get('force_reload'):
                        cls._instances.pop(rep)
                    else:
                        inst = cls._instances[rep]

                if inst is None:
                    # will need to make a new instance

                    # check existing representations (previously created at
                    # RunTime)
                    if not kwargs.get('usenameargs', True):
                        if rep in ReprInst._reprs:
                            repR = ReprInst._reprs[rep]
                            name = repR.name
                            kwargs.update(repR.kwargs)
                            kwargs['rep'] = str(repR.repr)
                        else:
                            pass

                    try:

                        # pre __new__
                        # -----------------------------------------------------------------------------

                        inst = __new__(cls, name, *args, **kwargs)

# -----------------------------------------------------------------------------
# post __new__

                    except BaseException:
                        if rep in cls._instances:
                            cls._instances.pop(rep)
                        if rep in ReprInst._reprs:
                            ReprInst._reprs.pop(rep)

                        # remove inst from parents if in any
                        parents = kwargs.get('parents', [])

                        for p in iterflatten(parents):
                            p_rep = p.as_repr()
                            p_rep._children.discard(rep)

                        raise

        # update parents - even if object is alrady alive
##        inst.parents = kwargs.get('parents')
        return inst

    return echo_func_storage__new__


def get_remote_loadfiles(cls, fullname, root, family, n=1):
    """ n = max number of loadfiles to search for in config[sources][remote_root]
    """

    remote_roots = Storage._multikey_static(cls, 'remote_root', family=family)

    if not remote_roots:
        warnings.warn('No remote_roots provided')

    remote_loadfiles = []

    try:

        root_src = cls._get_config_static(
            cls, '', 'STORAGE_ROOT', family=family)
        if not root_src:
            raise KeyError
        configdir = Storage.__storage__['configdir'][family]
        root_src = os.path.abspath(os.path.join(configdir, root_src))

    except KeyError:
        warnings.warn(
            '"STORAGE_ROOT" is not set in the config file. Using objects root instead.')
        root_src = root

    try:
        basename = fullname.split(root_src)[1].strip(os.sep)
    except IndexError:
        if DEBUG:
            print('problem splitting fullname', fullname, )
            print('root_src', root_src)
        # root_src is not part of fullname
        return []

    if DEBUG:
        print('remote roots', remote_roots)

    for r in remote_roots:
        root = os.path.abspath(r)
        src = os.path.join(root, basename)
        if DEBUG:
            print('local file', fullname)
            print('target remote file', src)

        if os.path.isfile(src):
            remote_loadfiles.append(src)

            if len(remote_loadfiles) == n:
                break
        elif DEBUG:
            print('File Not found')
    return remote_loadfiles


def __storage_init__(__init__):
    """ This decorator ensures the Storage object is correctly initialised.
    """
    @wraps(__init__)
    def echo_func_storage__init__(self, name, *args, **kwargs):

        if self.loaded == 'initialising':

            # pre __init__
            # -----------------------------------------------------------------------------
            try:

                __init__(self, name, *args, **kwargs)
# -----------------------------------------------------------------------------
        # post __init__

                if self.loaded == 'initialising':
                    raise Exception(
                        'Failed to initialise class correctly {0]'.format(self.__class__))

                elif self.loaded == True:

                    if not self.__storage__[
                            'cfgmaincls'] is self.__class__.__name__ and self.loadfile:
                        # only ignore warning for the the main class
                        # allow to 'load' if no loadfile

                        warnings.warn('This class uses an attribute'
                                      ' which loads it on __init__ which is bad -- Bug:{0}'.format(self.__class__))
            except BaseException:
                rep = repr(self)
                if rep in self._instances:
                    self._instances.pop(rep)
                if rep in ReprInst._reprs:
                    ReprInst._reprs.pop(rep)
                raise
                mesg = "Object {0} reported the following error\n{1}".format(
                    self.__class__, sys.exc_info()[1])
                raise sys.exc_info()[0](mesg)

    return echo_func_storage__init__


def defaults(func):
    """This decorator will load defaults from the configfile:
        cfgmainkey='default'
    """
    argnames = func.__code__.co_varnames[:func.__code__.co_argcount]
    fname = func.__name__
    fname_init = ['__init__', '__new__']

    if fname.find('__new__') > 0:
        fname = '__new__'

    elif fname.find('__init__') > 0:
        fname = '__init__'

    @wraps(func)
    def load_defaults(self, *args, **kwargs):

        if PRINT_DEFAULTS_MESSAGE:
            print('loading defaults for {0}'.format(fname))

        if isinstance(self, Storage):
            cls = self.__class__
            family = self.family

        elif issubclass(self, Storage):
            cls = self
            family = kwargs['family']

        else:
            raise TypeError('unknown class')

        # for special fname
        if fname in fname_init:
            fs = fname_init
        else:
            fs = [fname]

        for f in fs:
            cls._load_defaults(cls, f, argnames[1:len(args)],
                               kwargs, cls._cfgmainkey, family)

        return func(self, *args, **kwargs)
    return load_defaults


def calc_once(func):
    """
    Class decorator.
    Calculates the function and stores the result in the classs as the function name
    with a leading underscore.
    """
    funcName = "_propc1_" + func.__name__

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if not hasattr(self, funcName):
            setattr(self, funcName, func(self, *args, **kwargs))
        return getattr(self, funcName)
    return wrapper


def replaceStorageObjWithInst(obj):
    """ takes a list, tuple or dict and check for instances of Storage objects
    a new object will be created regardless of its type.
    """

    if isinstance(obj, dict):
        newobj = {}
        for k in obj:
            if type(k) in [dict, list, tuple]:
                k = replaceStorageObjWithInst(k)
            if type(obj[k]) in [dict, list, tuple]:
                newobj[k] = replaceStorageObjWithInst(obj[k])
            elif isinstance(obj[k], Storage):
                newobj[k] = ReprInst(obj[k])
            elif isinstance(k, Storage):
                newobj[ReprInst(k)] = obj.pop(k)
            else:
                newobj[k] = obj[k]

    elif isinstance(obj, list) or isinstance(obj, tuple):
        newobj = []
        for k in obj:
            if type(k) in [dict, list, tuple]:
                k = replaceStorageObjWithInst(k)
            if isinstance(k, Storage):
                newobj.append(ReprInst(k))
            else:
                newobj.append(k)
        newobj = type(obj)(newobj)

    elif isinstance(obj, Storage):
        newobj = ReprInst(obj)

    return newobj

# -----------------------------------------------------------------------------


class BunchMappable(BunchMappable):

    def __getattr__(self, name):
        return self[name]

    def __getitem__(self, key):

        try:
            return super(BunchMappable, self).__getitem__(key)
        except KeyError:

            parent = self
            depth = 0
            keys = [key]
            while hasattr(parent, '_parent'):
                depth += 1
                keys.append(getattr(parent, '_key', ''))
                parent = parent._parent or parent._mappable
            if not keys[-1]:
                keys.pop(-1)

            if isinstance(parent, configobj.Section):

                fname = parent.main.filename

                if parent.name:
                    description = '[{0}] of {1}'.format(parent.name, fname)
                else:
                    description = fname

                # make a pretty representation
                def n(i): return (len(keys) - i - 1)
                subs = '-->'.join(['{0}{1}{2}'.format('[' * n(i), k, ']' * n(i))
                                   for i, k in enumerate(reversed(keys))])

                raise KeyError("  {0}   is missing from:'{1}')".format(
                    subs, description))
            else:
                raise

    def _get(self, *keys):
        """ retrieve value from nested keys
        eg.
        self = {0:{1:2}}
        self._get(0,1)
        2
        """
        value = self
        for i in iterflatten(keys):
            try:
                value = value[i]
            except KeyError:
                raise
        return value

    # ToDo - do other dictionary type stuff and transfer to Parent object
    def keys(self):
        return [k for k in self]


class Bunch:
    """
    Often we want to just collect a bunch of stuff together, naming each
    item of the bunch; a dictionary's OK for that, but a small do- nothing
    class is even handier, and prettier to use.  Whenever you want to
    group a few variables:

      >>> point = Bunch(datum=2, squared=4, coord=12)
      >>> point.datum

      By: Alex Martelli
      From: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/52308
    """

    def __init__(self, **kwds):
        'remove operators'
        self.__items__ = {}
        self.__short_names__ = []
        for key, value in list(kwds.items()):
            key_alt = str(key).replace(' ', '').replace(
                '.', '').replace('=', '').replace('\\', '')
            self.__dict__[key_alt] = value
            self.__items__[key] = value
            self.__short_names__.append(key_alt)

    def get_dict(self):
        return self.__items__

    # todo insert an iterator fuction
    def __repr__(self):
        return '[%s]' % ', '.join(['%s=%s' % (s, self.__dict__[s])
                                   for s in sorted(self.__short_names__)])

    def __iter__(self):
        for value in list(self.__items__.values()):
            yield value


class Variables():
    """ Similar to Bunch, mainly ment to be used for control of variables in
    storage.variables
    but is really just a handy container to group variables
    keys are limited to Character strings of numbers, letters or underscores
    """

    def __init__(self, extern_dict, parent=None, **kwargs):
        self._e_dict = extern_dict
        self.store(**kwargs)
        self._parent = weakref.ref(parent)
        self.__dict__.update(self._e_dict)

    def __repr__(self):
        return '[%s]' % ', '.join(
            ['%s' % s for s in sorted(self._e_dict) if not s.find('_') == 0]
        )

    def __iter__(self):
        for value in self._e_dict:
            yield value

    def __getatt__(self, att):
        if att in self._e_dict:
            return self._e_dict[att]
        else:
            raise AttributeError("Variable not found:{0} parent={1}".format(
                att, repr(self._parent())))

    def store(self, **mappable):
        for key in mappable:
            if not isinstance(key, str):
                raise TypeError('%s is not type <str>' % type(key))
            ptn = '[^A-Z^a-z^0-9^=^_]'
            if re.search(ptn, key):
                raise NameError(
                    'variable name unsuitable:\nfound the following characters:\n%s' % re.findall(ptn, key))
            self._e_dict[key] = mappable[key]
            self.__dict__.update(self._e_dict)


class ReprInst(object):
    """ This is a light representation of a Storage instance.
    This object can be used to reconstruct the object if it is destroyed.
    """

    _reprs = {}  # WeakValueDictionary()
    _fullname_mapping = {}

    def __new__(cls, inst, **kwargs):

        if isinstance(inst, dict):
            rep = inst['repr']

            if rep in cls._reprs:
                return cls._reprs[rep]
            else:
                pass  # will have to build a new one so continue

        elif isinstance(inst, ReprInst):
            return inst

        elif isinstance(inst, Storage):
            # see if a representation already exists
            rep = repr(inst)

        # string representation
        elif (isinstance(inst, str) or isinstance(inst, unicode))and \
                inst in cls._reprs:
            return cls._reprs[inst]

        else:
            try:
                inst = eval(inst, Storage._objects)
            except BaseException:
                raise TypeError(
                    'Cannot represent this instance: {0}'.format(inst))

        if rep in cls._reprs:
            inst = cls._reprs[rep]

        # spawn a new instance
        else:
            inst = super(ReprInst, cls).__new__(cls)
        return inst

    def __init__(self, inst, **kwargs):
        """ Make a represenation of the instance so object may be
        retrieved at a later date - including parents and children
        """

        # check already been initialised
        if hasattr(self, 'name'):
            return

        self.kwargs = {}
        self.repr = repr(inst)
        self._parents = set()
        self._children = set()

        # a dict with ReprInst infor
        if isinstance(inst, dict):
            self.name = inst['name']
            self.cls = inst['cls']
            kwargs = inst['kwargs']

        # a Storage instance
        elif isinstance(inst, Storage):

            self.name = inst.name
            self.cls = inst.__class__.__name__
            kwargs['root'] = inst._root  # ToDo - look at this
            self._fullname_mapping[str(inst)] = self

        else:
            raise TypeError('"inst" object not allowed: {0}'.format(
                type(inst)))

        kwargs['usenameargs'] = False
        kwargsClean = replaceStorageObjWithInst(kwargs)
        self.kwargs.update(kwargsClean)
        # make sure Storage objects are not in the kwargs.
        # Which may otherwise cause unexpected Memory Leaks.

        # register as an object
        self._reprs[self.repr] = self

        if self.cls not in Storage._objects:
            Storage._objects[self.cls] = inst.__class__

    def __repr__(self):
        rep = 'ReprInst({0})'.format(str(dict(
            name=self.name,
            cls=self.cls,
            kwargs=self.kwargs,
            repr=self.repr))
        )
        return rep

    def get_cls(self):
        return Storage._objects[self.cls]

    def get_fullname_root_and_rep(self):

        cls = self.get_cls()
        return cls.get_fullname_root_and_rep(cls, self.name, **self.kwargs)

    @property
    def parents(self):
        return [ReprInst(p) for p in self._parents]

    @parents.setter
    def parents(self, parents):

        if not isinstance(parents, Storage) and parents in [None, '', []]:
            return

        if not isinstance(parents, list) and not isinstance(parents, tuple):
            parents = [parents]

        for p in parents:
            if isinstance(p, Storage):
                rep = repr(p)
            elif isinstance(p, ReprInst):
                rep = p.repr

            self._parents.add(rep)

            # register self as a child
            ReprInst(rep).children = self

    @property
    def family(self):
        return _get_family_from_repr(self.repr)

    @property
    def children(self):

        children = [ReprInst(c) for c in self._children]
        return children

    @children.setter
    def children(self, children=[]):

        if not isinstance(children, list) and not isinstance(children, tuple):
            children = [children]

        for c in children:
            if isinstance(c, Storage):
                rep = repr(c)
            elif isinstance(c, ReprInst):
                rep = c.repr

            self._children.add(rep)

    def get_inst(self):
        if self.repr in Storage._instances:
            return Storage._instances[self.repr]
        else:
            cls = Storage._objects[self.cls]
            return cls(self.name, **self.kwargs)


def getobjFromRep(rep):

    cls = rep[:rep.find('(')]
    if cls not in Storage._objects:
        raise KeyError('object not available: {0}'.format(cls))
    return eval(rep, Storage._objects)


def cfg_init(cfg, defaults):
    """
    Initialise the main config file with default values.

    Only performed once. This function is called when retriving the
    configuration files so can be used as a means of providing default values
    for the configuration files.

    Arguments:
    ----------
    cfg : Configobj | Dict
        mappings for configurations


    """

    if cfg.get('initialised'):
        return

    for key, val in list(defaults.items()):
        if key not in cfg:
            cfg[key] = val
    cfg['initialised'] = True


class Storage(object):
    """ A storage class which stores any data located in its dictionary
    Storage().data
    Only saves data as pickles, but this allows for some flexibility
    storage.root = '.' is the default root for all Storage instances
    a default root can be set using Storage.__storage__['root'] = root

    attributes can be stored in the name using notating:
        eg. h=12m_f=3.0Hz_
    _key=val_

    other useful properties are:
    Storage.notes     = 'Write your notes'

    Storage.variables.store(key=value)
    Storage.variables.key returns value


    You can also keep track of the subclasses provided the property ._type
    is set to something

    Storage.members_storage is a list of all members currently loaded in memory
    Storage.members_type is a list of all members with the same type
    Storage.relatives = a list of all related storage objects which are set by
    Storage.parents = parent where Storage.parent is the child

    for subclasses you should atleast set
    class new_class(Storage):

    def _gettype(self):
        return 'extension'
    _type = property(_gettype)
    """
    __storage__ = {}

    _instances = WeakValueDictionary()  # keep track of all storage objects in memory
    __storage__['configdir'] = {}  # {'default':os.getcwd()} # blank
    __storage__['cfgmaincls'] = 'Storage'
    __storage__['root'] = os.getcwd()
    _nameargs = []
    _configs = {}
    _cfgmainkey = 'default'
    _objects = {}
    _living_objs = {}
    _families = set()
    _remote_root_enabled = True
    _writemode = 'pickle'
    _compression = None

    # change this to suit different Classes (used as extension for savename)
    _type = ''

    @__storage_new__
    def __new__(cls, name, force_reload=False,
                usenameargs=True, local_only=False, **kwargs):
        """ main kwarg is root. if specified the file by name is looked for at that root.

        """

        if name in ReprInst._fullname_mapping:
            if DEBUG:
                print('Item found in ReprInst', name)
            fullname = name
            rep = ReprInst._fullname_mapping[fullname]
            name = rep.name
            root = rep.kwargs['root']

        fullname, rep, root = kwargs['_LOADINFO']
        instance_name = rep

        inst = cls._instances.get(instance_name, None)

        if inst and force_reload:
            cls._instances.pop(instance_name)

        # check for a failed initialisation
        if inst and (inst.loaded in ['initialising', 'loading!']):
            force_reload = True

        if force_reload and inst:
            inst = None

        if inst is None:
            # create a new one
            inst = super(Storage, cls).__new__(cls)
            inst._instance_name = instance_name
            cls._instances[instance_name] = inst

            inst._loaded = 'initialising'
            inst._root = root
            inst._fullname = fullname

            family = inst.family
            assert isinstance(family, str)
            inst._families.add(inst.family)

            # loadfiles
            if os.path.isfile(fullname):  # set the loadfile for new instances
                inst._loadfile = fullname

            elif inst._remote_root_enabled:
                # remote loadfiles

                remote_enabled = inst._get('remote_root_enabled')
                remote_disabled = inst._get('remote_root_disabled')

                if remote_enabled and not remote_disabled and not local_only:
                    loadfiles = inst._get_remote_loadfiles()
                    if DEBUG:
                        print('', loadfiles)

        # Register  inst and add any parents
        ReprInst(inst, **kwargs)

        # make sure object is registerable also
        if not cls.__name__ in cls._objects:
            cls._objects[cls.__name__] = cls

        return inst

    @__storage_init__
    def __init__(self, name=None, force_reload=False, family='', **kwargs):
        """
        kwargs include:
            root    - Storage root for object. Name may then be a subpath under root.
            family  - A means for grouping objects together.
        """

        if getattr(self, '_loaded', False) or force_reload:
            self.__reset__()

            config = kwargs.get('config')

            if isinstance(config, dict):
                self.config.update(config)

            elif isinstance(config, str):
                filename = os.path.abspath(config)
                self._config = ConfigObj(filename)

            elif isinstance(config, configobj.ConfigObj):
                self._config = config

    def __reset__(self):
        if hasattr(self, '_loaded'):
            del(self._loaded)

        self.__loaded_from_file__ = False
        self._data = {}
        self._data['store'] = {}
        self._data['notes'] = []

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.data.clear()
        return isinstance(value, TypeError)

    def __str__(self):
        return self.savename

    def __repr__(self):
        return self._instance_name

    def __del__(self):

        # tidy up family if last one
        if len(self.members_family) == 1:
            self._families.discard(self.family)

        rep = repr(self)
        if rep in self._instances:
            self._instances.pop(rep)

# -----------------------------------------------------------------------------
# properties

    @property
    def root_source_data(self):
        configdir = self.__storage__['configdir'][self.family]

        try:
            relpath = self._get_config_static(
                self.__class__, '', 'STORAGE_ROOT', family=self.family)
        except KeyError:
            warnings.warn(sys.exc_info()[1].args)
            return self.root

        root_source_data = os.path.abspath(os.path.join(configdir, relpath))

        if not os.path.isdir(root_source_data):
            mesg = "Path to Source data for {0} is incorrect. "\
                   "See config file: STORAGE_ROOT".format(self.__class__)
            raise IOError(mesg)
        return root_source_data

    @property
    def data(self):

        # automatic loading
        if not hasattr(self, '_loaded'):
            self._loaded = False
            self.load()
            self._loaded = True

        return self._data  # if this fails there is a problem in cls.__init__

    @staticmethod
    def _type_extension(cls):
        """ The extension for this cls including the type if it is used.
        """

        if cls._type:
            tp = TYPE_SEP + cls._type
        else:
            tp = ''
        tp = tp + _get_extension(cls._writemode)

        return tp

    @property
    def name(self):
        extn = self._type_extension(self)
        try:
            name = self._fullname.split(self.root)[1].split(extn)[
                0].strip(os.sep)
        except BaseException:
            name = os.path.basename(self._fullname).split(extn)[
                0].strip(os.sep)
        ext = '.' + self._type
        name = name.split(ext)[0]
        return name

    @name.setter
    def name(self, value):
        raise AttributeError("Renaming an Storage instance is not permitted!")

    @staticmethod
    def clear_all_made_objects():
        """ This method will purge all stored instantiated objects and
        representations.
        """
        ReprInst._reprs.clear()
        Storage._instances.clear()

    @staticmethod
    def _getfullname_and_root(cls, name, usenameargs=True, **kwargs):
        """
        generate the name and root for the cls object based on the name and
        the cls

        if usenameargs then any kwargs passed that are also in
        cls._nameargs are included in the name using the syntax
        name_key=value
        kwargs =
        if glob_list==True then if glob args are used then the glob result
        is returned
        """

        if isinstance(cls, str):
            cls = Storage._objects[cls]

        elif isinstance(name, cls):
            # already instantiated (same class)
            return name._fullname, name.root

        elif isinstance(name, Storage):
            name = name.name

        # Preference for obtaining the root is as follows:
        # 1. provided as a keyword
        # 2. provided as a configkey in the keywords
        # 3. Specified as the class root
        # 4. Inherited from a name object with the root attribute
        # 5. The storage root
        # 6. The current working directory [should never happen]

        try:
            family = kwargs['family']
        except KeyError:
            raise AttributeError(
                'keyword "family" must be provided for Storage type objects')

        root_by_configkey = ''
        if 'configkey' in kwargs:
            args = ['roots', kwargs['configkey']]

            try:
                root_by_configkey = cls._get_config_static(
                    cls, '', *args, **kwargs)
                root_family = cls.__storage__['configdir'][family]
                root_by_configkey = os.path.join(
                    root_family, root_by_configkey)
                root_by_configkey = os.path.abspath(root_by_configkey)
            except KeyError:
                if PRINT_CONFIG_MESSAGES:
                    print(sys.exc_info()[1].args[0])

        root = kwargs.get('root')\
            or root_by_configkey \
            or cls.__storage__.get(cls.__name__, {}).get('root')   \
            or getattr(name, 'root', '')\
            or cls.__storage__.get('root')   \
            or os.getcwd()

        # 2nd allows for links to other computers
        if not os.path.isabs(root) and root.find("\\\\") != 0:
            print('\n' * 10, cls.__storage__.get('root'), os.getcwd(), cls)
            raise IOError(
                'Passsing the root as a relative path is not permitted!: {0}'.format(root))

        # passed an object directly we need to strip the extension from the
        # object
        if isinstance(name, Storage):
            name = name.name

        name = str(name)
        if os.path.isabs(name):
            usenameargs = False
        else:
            name = os.path.abspath(os.path.join(root, name))

        tp = cls._type_extension(cls)

        # new style naming conventions (best practice to use this)
        if usenameargs is True:
            name_args = ['='.join((str(key), str(kwargs[key])))
                         for key in sorted(kwargs) if key in cls._nameargs]
            if name_args:
                name_args.insert(0, name.split(tp)[0])
                name = '_'.join(name_args)

        if os.path.isfile(name):
            return name, root
        else:
            return name.split(tp)[0] + tp, root

    @staticmethod
    def _getabsname(fullname, root):
        return os.path.splitext(os.path.relpath(fullname, root))[0]

    @staticmethod
    def getStorageRepr(cls, fullname, family, force_new):

        i = 0
        rep = None

        while rep in ReprInst._reprs or i == 0:
            i += 1
            rep = '{0}(r"{1}", family="{2}", instance={inst:d})'.format(
                cls.__name__,
                fullname,
                family,
                inst=i
            )
            if not force_new:
                break

            if i >= 1000:
                mesg = "Too many instances of the same object to continue!"\
                    " This is probably symptomatic of a sytax error elsewhere"
                raise RuntimeError(mesg)

        return rep

    @staticmethod
    def get_fullname_root_and_rep(
            cls, name, family, force_new=False, **kwargs):

        fullname, root = cls._getfullname_and_root(
            cls, name, family=family, **kwargs)
        rep = cls.getStorageRepr(cls, fullname, family, force_new)
        return fullname, root, rep

    @property
    def variables(self):
        if not hasattr(self, '_variables'):
            data = self.data
            self._variables = Variables(data['store'], parent=self)
        return self._variables

    @property
    def members_type(self):
        keys = [key for key in self._instances if key.find(
            self.__class__.__name__) == 0]
        vals = []
        for key in keys:
            try:
                vals.append(self._instances[keys])
            except BaseException:
                continue
        return vals

    @property
    def members_storage(self):
        return list(self._instances.values())

    @property
    def members_family(self):
        return [o for o in self.members_storage if o.family == self.family]

    @property
    def root(self):
        return self._root or self.__storage__.get(
            'root') or os.path.split(self._fullname)[0]

    @property
    def savepath(self):
        return os.path.abspath(os.path.join(self.root, self.name))

    @property
    def savename(self):

        extn = self._type_extension(self)
        return self.savepath + extn

    @property
    def relatives(self):

        def get_recursive(obj, attr):
            att = getattr(obj, attr)
            for a in att:
                if getattr(a, attr):
                    get_recursive(a, attr)
                yield a
            yield obj
        # get all parents and grand parents etc
        parents = []
        for parent in get_recursive(self, 'parents'):
            parents.append(parent)
        children = []
        for parent in parents:
            for child in get_recursive(parent, 'children'):
                if child not in children:
                    children.append(child)
        return [c for c in sorted(set(children))]


# notes
    @property
    def notes(self):
        """ prints the notes, does not return them!
        self.data['notes']
        """
        notes = ''
        for entry in self.data['notes']:
            notes += '_' * len(entry[1]) + '\n'
            notes += entry[0]
            notes += '\n'
            notes += str(entry[1])
            notes += '\n'
        print(notes)

    @notes.setter
    def notes(self, notes):

        self.data['notes'].append((ctime(), notes))

    @property
    def loadfile(self):
        loadfile = getattr(self, '_loadfile', '')

        if os.path.isfile(loadfile) or loadfile == '<remote>':
            return loadfile
        else:
            return ''

    @property
    def name_atts(self):
        if self.loadfile:
            name = self.loadfile
        else:
            name = self.savename
        return get_name_atts(name)

    @property
    def parents(self):
        objs = []

        rep = self.as_repr()

        for oRep in rep.parents:
            if oRep in self._instances:
                objs.append(self._instances[oRep])
            else:
                try:
                    inst = ReprInst(oRep).get_inst()
                except BaseException:
                    raise
                objs.append(inst)
        return objs

    @parents.setter
    def parents(self, pars):

        # -----------------------------------------------------------------------------
        rep = ReprInst(self)
        rep.parents = pars

    @property
    def family(self):
        # extract family name from instance_name
        family = _get_family_from_repr(self._instance_name)
        return family

    @property
    def children(self):
        rep = self.as_repr()
        objs = []
        for c in rep.children:
            try:
                objs.append(c.get_inst())
            except BaseException:
                mesg = "Unable to load child {0} for object {1}".format(
                    c, self)
                warnings.warn(mesg)

        return objs

    @property
    def _modtime(self):
        return os.stat(self.filename).st_mtime

# -----------------------------------------------------------------------------
# methods

    def as_repr(self):
        """ Make a representation of the object so that it may be loaded from scratch.
        Assuming the object has been saved or kept alive in memory.
        Importantly references to parents is maintained.
        """

        return ReprInst(self)

    def _objfromdict(self, **kwargs):
        raise DeprecationWarning('Super seeded. use ReprInst')

    def store_variable(self, xxx_todo_changeme):
        """ Legacy function for storing a variable
        new procedure is to use:
            this_obj.variables.store(key=value)
        """
        (key, value) = xxx_todo_changeme
        mappable = {key: value}
        self.variables.store(**mappable)
        return value

    def get_new_name(self, name='', force_new=True,
                     append_name=False, root=''):
        """
        Returns a name and root using available switching arguments and details
        about current object permitting nested folders.
        Arguments
        ---------
        name: str (optional)
            if

        force_new: bool
            Will ensure that an unused name is provided, otherwise the name
            may belong to an existing object.

        append_name: bool
            "name" will be appended to the name of the current object.
        root: str
            the root folder for the object to be stored


        new_name = self.savename + os.sep + new_name
        new_name is conditional on force_new and will append then next available %02d i

        Returns
        -------
        name, root

        """

        i = 0

        if not name or append_name:
            new_name = name = '_'.join((self.name, name))
        else:
            new_name = name

        root = root or self.root

        if not force_new:
            return new_name, root
        # check if the object exists

        rep = self.as_repr()

        cls = rep.get_cls()
        kwargs = {}

        kwargs.update(rep.kwargs)
        kwargs['root'] = root
        kwargs['allow_remote'] = False

        lim = 10e3  # more than this is probably a bug ?

        while True:
            fname, root, rep = cls.get_fullname_root_and_rep(
                cls, new_name, **kwargs)
            if (rep in Storage._instances) or os.path.isfile(
                    fname) or (rep in ReprInst._reprs):
                i += 1
                new_name = name + '_%02d' % i

                if i > lim:
                    raise IOError(
                        'Naming error, limited to {0} new objects'.format(lim))
            else:
                break

        return new_name, root

    @property
    def loaded(self):
        return getattr(self, '_loaded', False)

    def load(self, quiet=False, force_reload=False, **kwargs):

        if isinstance(self.loadfile, Storage):
            self._data.update(copy.deepcopy(self.loadfile.data))
            self._loaded = True

        if force_reload:
            self.__reset__()

        if self.loaded is True:
            return True

    # remote loadfiles
# -----------------------------------------------------------------------------

        if self.loadfile == '<remote>':
            save_after_load = True
            loadfiles = self._get_remote_loadfiles()
            if loadfiles:
                self._loadfile = loadfiles[0]
            else:
                raise IOError('Remote files are no longer available:{0}'.format(
                    self._remote_loadfiles))
        else:
            save_after_load = False
# -----------------------------------------------------------------------------

        if not self.loadfile:
            self._loaded = False
            return False
        else:
            try:
                data = read_file(self.loadfile)
            except EOFError:
                if not quiet:
                    raise EOFError(
                        'Error loading file "{0}" (file corrupted)'.format(self.loadfile))

            self._data.update(data)
            self.__loaded_from_file__ = True
            self._loaded = True

            if PRINT_LOADSAVE:
                if not quiet:
                    print('loaded {0} data from {1}'.format(
                        self.__class__.__name__, self.loadfile))

            if save_after_load:
                self.save()

            return True

    def save(self, dst='', quiet=False, comments='', track_changes=False):
        if not self.loaded:
            return

        if not dst:
            dst = self.savename
        if not dst:
            raise ValueError(
                'do not know were to save, either provide a dst, or set the root')
        else:
            write_file(self._data, dst, mode=self._writemode)
            if PRINT_LOADSAVE:
                if not quiet:
                    print('saved %s \tsize= %0.2f kbytes' %
                          (dst, (os.stat(dst).st_size / 1e3)))
        for child in self.children:
            try:
                child.save(quiet=quiet)
            except BaseException:
                pass

    def _cpth(self, *keys, **kwargs):
        '''Returns the absolute path of pth relative to self.config['STORAGE_ROOT']
            **kwargs - aslist. returns all possible options if a list was passed.
            Otherwise only the first option is returned as a string.
        '''

        value = self._get(*keys)
        pths = []
        root = self.root_source_data

        for f in iterflatten(value):
            pth = os.path.join(root, value)
            pth = os.path.abspath(pth)
            pths.append(pth)

        if kwargs.get('aslist'):
            return pths
        else:
            return pths[0]

    @property
    def config(self):
        return self._get_config(
            self.__class__, self.family, None, self._cfgmainkey)

    @staticmethod
    def _get_config(cls, family, name=None, cfgmainkey='', separate_config=''):
        if not isinstance(family, str):
            mesg = "'family' must be a string not a {0}".format(type(family))
            raise TypeError(mesg)
        cfgmainkey = family

        k = cls.__name__
        config_dir = cls.__storage__['configdir'].get(
            family, Storage.__storage__['root'])
        cfgmaincls = cls._objects[cls.__storage__['cfgmaincls']]

        def get_config(cls, name, separate_config=''):
            """ Separate config enables like information from different places to be stored
            """

            if separate_config:
                filename = os.path.join(
                    config_dir, '{0}.ini'.format(separate_config))
                default_fn = filename
                name = os.path.split(name)[-1]

            else:
                filename = os.path.join(
                    config_dir, make_config_name(cls, name or 'default'))
                default_fn = os.path.join(
                    config_dir, make_config_name(cls, 'default'))

            filename = os.path.abspath(filename)

            if not filename in cls._configs:
                cfg = ConfigObj(filename)
                cfg_d = ConfigObj(default_fn)
                cfg_d.update(cfg)
                cfg_d.filename = cfg.filename
                cls._configs[filename] = cfg_d

            if separate_config:
                # The name is inserted into the separate_config
                cfg = cls._configs[filename]

                # works on a heirachy. If a higher level entry exists then use it. checks for default also
                # 1st level or zeroth
                cfg = cfg.get(cfgmainkey, cfg.get('default', cfg))
                # 2nd level or first
                return cfg.get(name, cfg.get('default', cfg))
            else:
                return cls._configs[filename]

        if separate_config:
            return get_config(cls, name, separate_config)
        cfg_main = get_config(cfgmaincls, cfgmainkey)

        cfg_init(cfg_main, CFG_MAIN_DEFAULTS)

    # test if cls is the 'main' class in terms of configuration.
        if cls.__name__ == cfgmaincls.__name__:
            return cfg_main

# others with a config file
        has_separate_config_file = cfg_main.get('has_separate_config_file', '')
        if isinstance(has_separate_config_file, str):
            has_separate_config_file = str2list(has_separate_config_file)
        if k in has_separate_config_file:
            cfg = get_config(cls, family)
            return cfg
        else:
            # put an entry in the main config (or get the separate config with
            cfg = get_config(cfgmaincls, cfgmainkey)
            if k not in cfg:
                cfg[k] = {}
            return cfg[k]

    @property
    def _cfg(self):
        if self._type not in self.config:
            self.config[self._type] = {}
        sub = self.config[self._type]

        if self.name not in sub:
            sub[self.name] = {}
        return sub[self.name]

    def _get(self, *keys, **kwargs):
        """ retrieve value from the config file key or nested keys
        eg.
        self.config = {0:{1:2}}
        self._get(0,1)
        2

        kwargs:
            asconfig = True - will return the config object segment which may used as a regular segment

        """
        cfg = self._get_config_static(
            self.__class__, self._cfgmainkey, *keys, family=self.family, **kwargs)
        if keys:
            return evalAttributes(self, cfg)
        else:
            return cfg

    @staticmethod
    def _get_config_static(cls, cfgmainkey='', *keys, **kwargs):
        """ retrieve value from the config file key or nested keys
        eg.
        cls.config = {0:{1:2}}
        cls._get(0,1)
        2
        kwargs:
            asconfig        = True
            separate_config
        """
        separate_config = kwargs.get('separate_config', False)
        name = kwargs.get('name', cls.__name__)
        family = kwargs['family']
        if not isinstance(family, str):
            mesg = "'family' must by type <str> not {0}".format(type(family))
            raise TypeError(mesg)

        config = cls._get_config(
            cls, family, name, cfgmainkey, separate_config)
        value = config
        exec_info = []
    # look in own
        try:
            for i in iterflatten(keys):
                try:
                    value = value[i]
                except (KeyError, TypeError):
                    # raise the error with a pointer to where it is missing
                    # from
                    nested = BunchMappable(config)
                    value = nested._get(*keys)

            if kwargs.get('asconfig'):
                return value
            else:
                return str2num(value)
        except BaseException:
            exec_info.append(sys.exc_info())

            cfgmaincls = cls._objects[cls.__storage__['cfgmaincls']]

            if not cls.__name__ == cfgmaincls.__name__:
                # try in base classes also [above should prevent infinite
                # loop?]
                for c in cls.__bases__:
                    try:
                        return cls._get_config_static(
                            c, cfgmainkey, *keys, **kwargs)
                    except BaseException:
                        exec_info.append(sys.exc_info())
                        pass
                try:
                    # look in main class
                    return cls._get_config_static(
                        cfgmaincls, cfgmainkey, *keys, **kwargs)
                except BaseException:
                    exec_info.append(sys.exc_info())
                    pass

    # Something is missing. Extract the error information and raise
            message = exec_info[0][1].args[0]
            if len(exec_info) > 1:
                message = 'Either' + message
                errors = [exec_info[-1]]
                message = message + ' or '.join([e[1].args[0] for e in errors])
            raise exec_info[0][0](message)

    @staticmethod
    def _load_defaults(cls, fname, argnames=[], kwargs={},
                       cfgmainkey='default', family=None):
        """ Load the default values into kwargs by checking the configfile
        """

        # get defaults. stored under. DEFAULTS
        try:

            d = cls._get_config_static(
                cls, cfgmainkey, 'DEFAULTS', fname, family=family)

        except KeyError:
            if RAISE_CONFIG_ERRORS:
                raise

            d = []
            if PRINT_DEFAULTS_MESSAGE:
                print(sys.exc_info()[1].args[0])

        keys = set(d).difference(argnames).difference(kwargs)
        for k in keys:
            if k in d:
                kwargs[k] = d[k]
            elif PRINT_DEFAULTS_MESSAGE:
                print('missing "{0}" Default value from: {1} in {2}'.format(
                    k, fname, cls))

    @staticmethod
    def _multikey_static(cls, *keys, **kwargs):
        """ From the configuration file for the cls retrive from the sources
            entry a maximum of n "key" entries.
            eg. sources->remote_root
                which is numbered sequentially. starting at 1

            key = '{key}{sep}{0:d}'

        """
# get a list of remote roots
#-------------------------------------------------------------------------

        sep = kwargs.get('sep', '-')
        family = kwargs['family']

        sources = []

        args = list(keys)
        k = keys[-1]
        i = 0

        while True:
            if i:
                k = '{key}{sep}{0:d}'.format(i, key=keys[-1], sep=sep)
            try:
                args[-1] = k
                root = cls._get_config_static(cls, '', *args, family=family)
                sources.append(root)

            except KeyError:
                break
            i += 1

        return sources

    def _get_root(self, configkey, i=0, basekey='sources', passive=False):
        """ Retrieve root from config file. i permits a suffix value to be provided.
        Suffix is an integer greater than zero
        """

        if i:
            n = configkey + '-{0:d}'.format(i)
        else:
            n = configkey
        try:
            return self._cpth(basekey, n)
        except KeyError:
            if not passive:
                raise

    def get_obj(self, cls, name, root='', force_recalc=False,
                glob_list=False, keep_alive=False, **kwargs):
        """ Try to instantiate cls(name, root)
            - checks alternate sources from the configfile under self.config['sources'][key]
            where key is configkey or configkey-###. Where ### is numbered sequentially starting at 1.

        force_recalc- raise an error
        copy        - Copy the file to root.
        glob_list   - Return the glob result instead of the instance. requires: glob.has_magic(name)==True

        configkey   - optional for
        """

        if 'family' not in kwargs:
            kwargs['family'] = self.family

        if cls in self._objects:
            cls = self._objects[cls]

        if isinstance(cls, Storage):
            cls = cls.__class__

        if not issubclass(cls, Storage):
            raise TypeError(
                'Object is likely not compatible for retrieval: {0} [{1}]'.format(cls, name))

        if glob_list:
            return self._getfullname_and_root(
                cls, name, root=root, glob_list=glob_list, **kwargs)

        elif force_recalc:
            raise IOError('Force recalc for: {0} - {1}'.format(cls, name))

        elif isinstance(cls, str):
            raise KeyError('"{0}" is not listed as an _object'.format(cls))

        else:
            inst = cls(name, root=root, **kwargs)

            if keep_alive:
                self._living_objs[repr(inst)] = inst
            return inst

    def _get_remote_loadfiles(self, n=1):

        remote_loadfiles = get_remote_loadfiles(
            self.__class__, self._fullname, self.root, self.family, n)

        if remote_loadfiles:
            self._remote_loadfiles = remote_loadfiles
            if not self.loadfile:
                self._loadfile = '<remote>'

        return remote_loadfiles

    def get_remote_inst(self, **kwargs):

        self._get_remote_loadfiles()

        rep = self.as_repr()
        kwargs = {}
        kwargs.update(rep.kwargs)
        kwargs.update(kwargs)
        kwargs['copy'] = False
        kwargs['remote_only'] = True

        return self.get_obj(self, self.name, **kwargs)


Storage._objects['Storage'] = Storage
Storage._objects['ReprInst'] = ReprInst


def _update_config_defaults(configfile='defaults.ini', obj_in_folder=None):
    import inspect
    if not os.path.isabs(configfile):
        folder = os.path.dirname(inspect.getfile(obj_in_folder))
        configfile = os.path.join(folder, configfile)

    config = ConfigObj(configfile)
    CFG_MAIN_DEFAULTS.update(config)


_update_config_defaults('defaults.ini', Storage)
