
import os
import sys
import numpy as np

if __name__ in ['__main__', 'test_IM']:
    pth = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    sys.path.insert(0, pth)

import IM
from IM import *


#-------------------------------------------------------------------------

from numpy.testing import (TestCase, assert_almost_equal, assert_equal,
                           assert_, assert_raises, run_module_suite,
                           assert_allclose)

import shutil
from tempfile import mkdtemp
import gc
import glob
#-------------------------------------------------------------------------
import ReadIM
files = ReadIM.extra.get_sample_image_filenames() + \
    ReadIM.extra.get_sample_vector_filenames()

src_dir = ReadIM.extra.get_sample_folder()


def get_vec(factor=None, **kwargs):
    """ Returns a demo vector
    """
    v = IM.demo_VC7(**kwargs)
    if factor is not None:
        for ax in v.axes:
            ax[:] = factor
    return v


vectype_2D = [ReadIM.core.BUFFER_FORMAT_VECTOR_2D,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
              ]


def get_bfs():
    """
    Iterate through buffer formats available
    """
    for bf in ReadIM.BUFFER_FORMATS:
        if bf > 0 and bf not in vectype_2D:
            "buffer format not yet supported {0}".format(
                ReadIM.BUFFER_FORMATS[bf])
            continue
        yield bf


class TestIM(TestCase):

    def setUp(self):

        self.pth = os.getcwd()
        self.tempdir = mkdtemp()

        IM.common.Storage.__storage__['root'] = self.tempdir
        os.chdir(self.tempdir)

    def tearDown(self):
        os.chdir(self.pth)
        shutil.rmtree(self.tempdir)
        gc.collect()

    def testImaginaryProduct(self):
        v = IM.demo_VC7(dtype='complex')
        v[:] = -1
        v[:, 1:, 1] = -3
        v2 = v * 2
        assert hasattr(v2.Vx, 'real')

        im = IM.demo_IM7(dtype='complex')
        im2 = im * 2
        assert hasattr(im2.I, 'real')


if __name__ == '__main__':
    test = TestIM('testImaginaryProduct')
    test.debug()

    im = IM.demo_VC7()
    v1 = im.new_vc7()
    v2 = im.new_vc7()
    assert_(v1 is not v2)

# run_module_suite()
