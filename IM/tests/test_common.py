
import os
import sys

if __name__ in ['__main__', 'test_common']:
    pth = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    sys.path.insert(0, pth)

#-------------------------------------------------------------------------

from numpy.testing import (TestCase, assert_almost_equal, assert_equal,
                           assert_, assert_raises, run_module_suite,
                           assert_allclose)


import shutil
from tempfile import mkdtemp

#-------------------------------------------------------------------------

from IM.common import *
from IM.common import __storage_new__, __storage_init__, defaults, ReprInst


import IM.common
IM.common.DEBUG = True
import gc


class TestStorage(TestCase):

    def setUp(self):

        gc.collect()
        IM.common.DEBUG = True
        self.pth = os.getcwd()
        self.tempdir = mkdtemp()

        Storage.__storage__['root'] = self.tempdir
        os.chdir(self.tempdir)

    def tearDown(self):
        os.chdir(self.pth)
        shutil.rmtree(self.tempdir)
        gc.collect()

    def test_1_storage(self):
        """
        Test functionality of base Storage object
        """
    #-------------------------------------------------------------------------
    # an example of using the Storage class
        import numpy as np
    # example loading a variable and saving
        Storage.root = self.tempdir
        Storage._nameargs.append('NameArgument')
        s = Storage('s', NameArgument='hello', family='myFAMILY')
        s = Storage(s)
        s.data['somedata'] = np.ones(5)
        s.variables.store(name=3)
        s.save()

        child = Storage('child', family='myFAMILY')
        child.parents = s
        assert_((len(s.children) == 1))
        assert_((child in s.children))
        child.data['childs data'] = 'anything'

        s.save()
        assert_((os.path.isfile(str(child)),
                 'When the parent is saved the child should be saved'))

        s_str = str(s)
        assert_(os.path.isfile(s_str), s_str)
        rep_s = repr(s)

        del(s)
        del(child)
        import gc
        gc.collect()
        assert_(rep_s not in Storage._instances)

    # test loading from a file
        s = Storage('s', NameArgument='hello')
        assert_(not s.loaded,
                's should have been re-initialised so should not be loaded')

        assert_(s.loadfile)
        assert_(s.loadfile, 'Problem in __init__')
        assert_('somedata' in s.data)  # autoloading
        assert_('name' in s.variables, 'variables type data missing')
        assert_('NameArgument' in s.name_atts, '')

    # using representations
        rep = repr(s)
        assert_(s is eval(rep, Storage._objects))

        del(s)
        gc.collect()

        s = eval(rep, Storage._objects)
        assert_('name' in s.variables)

    # parents
        r = Storage('testing2')
        s.parents = r

        assert_(r in s.parents)
        assert_(s in r.children)

        assert_(r in s.parents)

        del(r, s)

    def test_2_a_new_object_and_complex_operations(self):
        """ Test the funcitonality of a new subclass of Storage with named arguments
        and test more complex behaviour including:
            named arguments
            remote object retrieval
            conversion of object to a dictionary and loading via dictionary
            keeping objects alive (belongs to spawning object)
        """
        # a new class
        # demo of storage usage
        class MyNewClass(Storage):
            _type = 'new'  # this is used as the extension. If it uses the same sort
            # of data as anther object then you can use it's type.

            # if NameArgs are passed when instantiating
            _nameargs = ['NameArg']
            # the object then these will be included
            # in the objects name.

            # this is optional if you need to override
            @__storage_new__
            @defaults
            def __new__(cls, name, k=2, force_new=True, **kwargs):
                assert_(k == 20)
                assert_('def' in kwargs, 'Failed to pass default')
                inst = super(MyNewClass, cls).__new__(
                    cls, name, force_new=force_new, **kwargs)
                return inst

            @__storage_init__
            @defaults
            def __init__(self, name, *args, **kwargs):
                assert_('k' in kwargs, 'Failed to pass default')
                super(MyNewClass, self).__init__(name, *args, **kwargs)

            @defaults  # defaults enables you to set defaults in the config file
            def hello(self, required='not', setdefault=False, **kwargs):
                return setdefault

        RAISE_CONFIG_ERROR = False
        # a dummy config file

        Storage('anything').config['DEFAULTS'] = {'__new__': {'k': 20, 'def': True},
                                                  'hello': {'setdefault': True}
                                                  }

        # a customised mapping in _objects
        key = 'see_a_new_class'
        s = MyNewClass(key, NameArg='YES')
        assert_(str(s).find('YES') > 0)

        MyNewClass._objects[key] = s

        assert_(s.hello())
        s.config
        MyNewClass(s)
        assert_(s.get_obj(MyNewClass, 'dummy') is s.get_obj(key, 'dummy'))
        try:
            s.get_obj(3234, 'failed')
        except TypeError:
            pass

        assert_(s is s.get_obj(s, s))
        assert_(s is s.get_obj(s, s.name))
        assert_(str(s) in ReprInst._fullname_mapping, 'mapping missing')

    def test_3_remote(self):
        IM.common.DEBUG = True

        # subclass
        class MyNewClassRemote(Storage):
            pass

        root = self.tempdir
        assert_equal(root, MyNewClassRemote.__storage__['root'])

        remote_root = os.path.join(root, 'remote_directory')

        # create a "remote file"
        ss = MyNewClassRemote('remote', NameArg='YES',
                              root=remote_root, force_new=True)
##        ss._root = remote_root
        remote_root = ss.root

        print(ss)
        assert_equal(ss.root, remote_root)

        assert_equal(str(ss).find(remote_root), 0,
                     'file path not set correctly')

        ss.variables.store(stored='Some data')

        # set the remote
        ss.config['remote_root'] = remote_root
        ss.config['remote_root_enabled'] = True
        ss.save()
        rep = repr(ss)
        name_remote = ss.savename
        assert_equal(name_remote, str(ss))
        name_short = ss.name

        print('saved remote file:', name_remote)

        assert_(os.path.isfile(name_remote), name_remote)

        del(ss)
        gc.collect()
        assert_(rep not in Storage._instances, 'Object should be gone')

        print('root = ', root)

        ss = MyNewClassRemote(name_short, root=root, force_new=True)

        print('local file', str(ss))
        assert_(ss._get('remote_root_enabled'))

        assert_equal(ss._get('remote_root'), remote_root,
                     'remote_root not in config')

        assert_equal(ss.name, name_short, 'remote file not found')

        assert_equal(ss.loaded, False, 'Should not be loaded yet')

        assert_equal(os.path.isfile(ss.loadfile), False,
                     'File should not exist locally')
        assert_equal(ss.loadfile, '<remote>',
                     "This test sometimes fails in unittest...")

      # load remote file and check the data
        assert_('stored' in ss.variables)
        assert_(os.path.isfile(str(ss)), 'File should be copied on demand')
        os.remove(str(ss))

        assert_(os.path.abspath(ss.loadfile) == name_remote,
                'This file did not come from the correct root?')
        # test data is saved and loaded locally
        ss.save()

        name_local = str(ss)
        rep_local = repr(ss)
        assert_(os.path.isfile(name_local))
        rep = ss.as_repr()

        # reload object
        del(ss)
        gc.collect()

        ss = MyNewClassRemote(name_short, root=root, force_new=True)

        # test load from file as usual
        assert_(ss.loadfile == name_local)

        # test load local from rep
        del(ss)
        gc.collect()

        ss = rep.get_inst()
        assert_(str(ss) == name_local)

        os.remove(name_local)

        # test load remote from rep

        del(ss)
        gc.collect()

        ss = rep.get_inst()
        assert_(ss.loadfile == '<remote>')
        assert_('stored' in ss.variables)
        assert_(os.path.abspath(ss.loadfile) == name_remote,
                'This file did not come from the correct root?')

        # test load remote from representation of RepInst

        os.remove(str(ss))
        del(ss)
        gc.collect()

        rep_r = (repr(rep))
        rep = eval(rep_r, Storage._objects)
        ss = rep.get_inst()
        assert_(not ss.loaded)
        assert_(ss.loadfile == '<remote>')
        assert_('stored' in ss.variables)

        # test loading an object from a local representation of itself from a remote
        # object using only its local representation

        ss_rep = repr(ss)

        os.remove(str(ss))
        del(ss)
        gc.collect()
        ss = eval(ss_rep, Storage._objects)
        assert_(ss.loadfile == '<remote>')
        assert_('stored' in ss.variables)

        # This may demonstrate the need to keep this information alive?
        os.remove(str(ss))
        del(ss)
        ReprInst._reprs.pop(ss_rep)
        gc.collect()
        ss = eval(ss_rep, Storage._objects)
        assert_(ss.loadfile == '<remote>')
        assert_('stored' in ss.variables)

        # disable remote
        ss.config['remote_root_disabled'] = True
        os.remove(str(ss))
        del(ss)
        ReprInst._reprs.pop(ss_rep)
        gc.collect()
        ss = eval(ss_rep, Storage._objects)
        assert_(not ss.loadfile, 'should have been disabled')
        ss.config['remote_root_disabled'] = False
        ss._get_remote_loadfiles()
        assert_(ss.loadfile == '<remote>')

        # test keeping objects alive
        def test_extended_functionality():
            m = Storage('m')
            r1 = m.get_obj(m, 'm1', keep_alive=False).as_repr()
            d2 = m.get_obj(m, 'm2', keep_alive=True).as_repr()

            class MyNewClass(Storage):
                pass

    # DISABLED
    # m2 = MyNewClass2(m) # A sub class will clone data from a superclass
    # del(m2)

            # Test object stays alive
            r2 = d2.get_inst()
            del(d2)
            gc.collect()
            assert_(repr(r2) in Storage._instances, 'Object not kept alive')

            # Test object does not stay alive
            assert_(repr(r1) not in Storage._instances,
                    'should have dissolved')
            m1 = r1.get_inst()  # can rebulid object even if it was not in existance
            assert_(r1 is m1.as_repr(),
                    'should have returned the same Repr object')

            # Test setting a parent at instantiation
            m2 = MyNewClass('mw', parents=m)
            assert_(m in m2.parents)

            # Test adding a parent even if the object is alive
            assert_(m1 not in m2.parents, 'm1 should not be a parent yet')
            m2 = MyNewClass('mw', parents=m1)
            assert_(m1 in m2.parents, 'm1 should have been added as a parent')

            # Add a representation of an object as a parent
            m3 = MyNewClass('m3')
            r1 = m1.as_repr()
            m3.parents = r1
            assert_(m1 in m3.parents,
                    'm1 should have been added as a parent')
            assert_(m3 in m1.children,
                    'm3 should have been added as a child')

            nn, root = m3.get_new_name('carrots')

            assert_equal(nn, 'carrots')

            return r1  # A test to make sure object is removed

        r1 = test_extended_functionality()
        gc.collect()

        assert_(repr(r1) not in Storage._objects, 'This object was kept alive')

    def test_children(self):
        a = Storage('a')
        b = Storage('b')
        b.parents = a

        a_new = Storage('a', force_new=True)

        assert_(not any(a_new.children),
                'Children should not belong to forced reloaded object')


if __name__ == "__main__":

    if False:
        config = {'a': 'a'}
        s = Storage('1', config=config)
        b = Storage('2')
        assert('a' in b.config)

    tst = TestStorage('test_3_remote')
    tst.debug()

# tst.setUp()
##
# tst.test_1_storage()
# tst.test_2_a_new_object_and_complex_operations()
# tst.test_3_remote()

# run_module_suite(argv=['-f',])
