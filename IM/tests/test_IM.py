
import os
import sys
import numpy as np

if __name__ in ['__main__', 'test_IM']:
    pth = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    sys.path.insert(0, pth)

import IM
from IM import *


#-------------------------------------------------------------------------

from numpy.testing import (TestCase, assert_almost_equal, assert_equal,
                           assert_, assert_raises, run_module_suite,
                           assert_allclose)

import shutil
from tempfile import mkdtemp
import gc
import glob
#-------------------------------------------------------------------------
import ReadIM
files = ReadIM.extra.get_sample_image_filenames() + \
    ReadIM.extra.get_sample_vector_filenames()

src_dir = ReadIM.extra.get_sample_folder()


def get_vec(factor=None):
    """ Returns a demo vector
    """
    v = IM.demo_VC7()
    if factor is not None:
        for ax in v.axes:
            ax[:] = factor
    return v


vectype_2D = [ReadIM.core.BUFFER_FORMAT_VECTOR_2D,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED,
              ReadIM.core.BUFFER_FORMAT_VECTOR_2D_EXTENDED_PEAK,
              ]


def get_bfs():
    """
    Iterate through buffer formats available
    """
    for bf in ReadIM.BUFFER_FORMATS:
        if bf > 0 and bf not in vectype_2D:
            "buffer format not yet supported {0}".format(
                ReadIM.BUFFER_FORMATS[bf])
            continue
        yield bf


class TestIM(TestCase):

    def setUp(self):

        self.pth = os.getcwd()
        self.tempdir = mkdtemp()

        IM.common.Storage.__storage__['root'] = self.tempdir
        os.chdir(self.tempdir)

    def tearDown(self):
        os.chdir(self.pth)
        shutil.rmtree(self.tempdir)
        gc.collect()

    def test__setitem__(self):
        v = get_vec()
        v[:] = -1
        v[:, 1:, 1] = -3

    def test__getitem__(self):
        v = get_vec()
        v2 = v[:]
        assert_((v2 == v).all())
        v3 = v[0]
        assert_equal(v.shape[1:], v3.shape[1:])
        v4 = v[1:2]
        assert_equal(v.shape[1:], v4.shape[1:])
        v5 = v[[0, 1]]
        assert_equal(v.shape[1:], v5.shape[1:])

    def testVC7_2C(self):

        # check files are available first
        filename_v = os.path.join(src_dir, '2C.VC7')
        assert os.path.isfile(filename_v)

    # read vector
    #-------------------------------------------------------------------------

        v = IM.VC7(filename_v)

    # store vector data
        assert v.attributes
        print('savename', v.savename)

        assert not os.path.isfile(v.savename)

        v.save()
        assert os.path.isfile(v.savename)
        os.remove(v.savename)

    # write vector to vc7
        v.writeVC7('test')

        v - v

        v.show()

        v1 = IM.demo_VC7()

        v1.writeVC7('test')

        assert isinstance(
            IM.VC7(v), IM.VC7), 'Somthing wrong in __new__ for obj VC7'

        v.show()
        reps = [repr(v) for v in [v, v1]]
        del(v)
        del(v1)

        gc.collect()
        for r in reps:
            assert_(r not in IM.IM7._instances,
                    "failed to delete object: {0}".format(r))

    def testVC7_3C(self):

        # check files are available first
        filename_v = os.path.join(src_dir, '3C.VC7')
        assert os.path.isfile(filename_v)

    # read vector
    #-------------------------------------------------------------------------

        v = IM.VC7(filename_v)

    # store vector data
        assert v.attributes
        print('savename', v.savename)

        assert not os.path.isfile(
            v.savename), "file already exists " + v.savename

        v.save()
        assert os.path.isfile(v.savename)
    # write vector to vc7
        newdir = v.writeVC7('test')
        filename = glob.glob(newdir + '/*.vc7')[0]

        v2 = IM.VC7(filename)
        assert_((v2 == v).all(), 'Problem writing file')

        v - v

        v.show()

        assert_(isinstance(IM.VC7(v), IM.VC7),
                'Somthing wrong in __new__ for obj VC7')

        v.show()

    def testIM7_1_cam(self):

        filename_im = os.path.join(src_dir, '1camera.im7')
        assert os.path.isfile(filename_im)

        im = IM.IM7(filename_im)
        im.show()

    def testIM7_2_cam(self):

        filename_im = os.path.join(src_dir, '2cameras.im7')
        assert os.path.isfile(filename_im)

        im = IM.IM7(filename_im, dtype=np.uint16)
        assert im.I.dtype.name == 'uint16'
        im.show()
        im._im7_writer('test.im7')

    def test_writeVC7(self):

        v = IM.demo_VC7()
        v._root = self.tempdir

        # test write vector
        v.writeVC7('writevectors')
        import glob
        ptn = os.path.join(self.tempdir, 'writevectors', '*.vc7')

        src = glob.glob(ptn)[0]
        vv = IM.VC7(src)
        vv.load()
        assert_(v.loadfile != vv.loadfile)
        v0 = v[0]
        mesg = 'The saved vector should have a single frame and same nx and ny'\
            ' as the original vector'
        assert_equal(v0.shape, vv.shape, mesg)
        v.show()
        vv.show()

        (v[0] - vv).show()

        assert_almost_equal(v[0].Vx, vv.Vx)
        assert_almost_equal(v[0].Vy, vv.Vy)

    def test_sse(self):

        # check files are available first
        filename_v = os.path.join(src_dir, '2C.VC7')
        assert os.path.isfile(filename_v)

        v = IM.VC7(filename_v)

        # v with uncertainty as a float
        vu = IM.VC7u(v.name, buff=v.buffer, sse_type='float')
        vu.variables.store(sse=2.2)
        assert vu.sse == 2.2

        # v with uncertainty as Vector
        vu2 = IM.VC7u(v.name + '2', buff=v.buffer, sse_type='VC7')
        vu2.sse.Vx[:] = 2.2
        vu2.sse.Vy[:] = 2.2

        assert_((vu2.sse.mag().I == (2.2**2 * 2)**0.5).all())  # vector sum

        vu.data.update(v.data)
        assert vu is not v, 'Uncertainty vector should be a different object'
        assert v.attributes

    def test_im7_writer(self):

        filename_im = os.path.join(src_dir, '1camera.im7')
        assert os.path.isfile(filename_im)

        im = IM.IM7(filename_im)

        # Test write image
        im._im7_writer('test.im7')
        im = IM.IM7('test.im7')
        repim = repr(im)
        assert im is eval(
            repim, IM.common.Storage._objects), 'a representation must be the same object'
        IM.common.PRINT_LOADSAVE
        im.data
        im.save()
        assert os.path.isfile(str(im))
        assert im.attributes

    def test_mag(self):
        for obj in [IM.demo_VC7(), IM.demo_IM7()]:
            obj[:] = 2
            mag = (2**2 * len(obj.axes))**0.5
            omag = obj.mag().I
            assert_((omag.mask == False).all(), 'Masking issue')
            assert_((omag == mag).all(), 'Error calculating magnitude')

    def test__nonzero__(self):
        for obj in [IM.demo_VC7(), IM.demo_IM7()]:
            obj[:] = 1
            assert_(obj, 'Object with data should be nonzero')
            obj[:] = 0
            assert_(
                not obj, 'Object without data should be nonzero. Except if loadfile')
            # test a non-zero will be true for a file
            obj[:] = 1
            obj.save()
            fname = str(obj)
            cls = obj.__class__
            repr(obj)

            obj2 = cls(fname, force_reload=True)

            assert_(obj2, 'an unloaded object with a loadfile should be nonzero')
            assert_(
                not obj2.loaded,
                'Previous test not valid if file is loaded')

    def test_makemask(self):
        for bf in get_bfs():

            gc.collect()
            buff1 = ReadIM.newBuffer([[0, 10], [10, 0]], 10, 10,
                                     frames=2, image_sub_type=bf,
                                     vectorGrid=1)

            if bf > 0:
                cls = IM.VC7
            else:
                cls = IM.IM7

            v1 = cls('v1_{0}'.format(bf), buff=buff1)
            v1[:] = 1

            v1.mask_coordinates['b1'] = [
                [1, 1], [1, 2], [2, 2], [2, 1], [1, 1]]
            total = sum(v1.sum(None))
            mask = v1.make_mask('b1')
            assert_equal(mask.sum(), 96)

    def test_newobj(self):

        v = IM.demo_VC7()
        im1 = v.new_im7()
        im2 = v.new_im7()
        assert_(im1 is not im2, 'new objects should be created')


if __name__ == '__main__':
    test = TestIM('testVC7_2C')

# test.debug()
