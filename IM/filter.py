
#-------------------------------------------------------------------------
# Name:        IM.filter.py
# Purpose:     Additional filtering functions specific for IM obects
# Author:      Alan Fleming
#
# Created:     7/06/2011
# Copyright:   (c) Alan 2011
#-------------------------------------------------------------------------
#!/usr/bin/env python

__metaclass__ = type

import ReadIM
from .core import VC7, IM7
from .core import loader as IMloader
import os
import numpy as np
from scipy import ndimage, signal
from scipy.interpolate import griddata
import itertools
from .common import get_name_atts, str2num, STR_TYPE
import re
from operator import itemgetter
import collections
import copy


# Globals
#-------------------------------------------------------------------------

__SHORTEN_NUM_SEQ__ = True

#-------------------------------------------------------------------------


def make_common_buffer(ims, dx=None, dy=None, vectorGrid=None):
    """
    Creates the smallest buffer that will accommodate all of the ims
    if dx, or dy are not provided, they are taken from the from the first im in ims.
    Useful for creating a mosaic.

    Parameters
    ---------
    ims: [VC7 | IM7]
        List of IM subclass objects with buffer and dx, dy attributes
    dx: float
        Pixel width.
    dy: float
        Pixel height.
    Notes:
    ------
        vector grid is taken from the first object in the list. This may caused
        undesired behaviour when a mix of IM7 and VC7 objects are used.

    Returns
    -------
    buff: bufferTypeAlt

    """
    if dx is None:
        dx = ims[0].dx
    if dy is None:
        dy = ims[0].dy

    windows = [v.window for v in ims]
    w1, w2 = np.min(windows, 0), np.max(windows, 0)
    window = ((w1[0, 0], w2[0, 1]), (w2[1, 0], w1[1, 1]))

    nx = int((window[1][0] - window[0][0]) / dx)
    ny = int((window[0][1] - window[1][1]) / dy)

    v = ims[0]
    vectorGrid = vectorGrid or v.buffer.vectorGrid
    buff = ReadIM.newBuffer(window, nx, ny, v.buffer.vectorGrid,
                            v.buffer.image_sub_type, frames=v.frames)

    # transfer buffer info from v. Useful for writing files.
    for a in ['I']:
        for b in ['factor', 'offset']:
            exec('buff.scale%s.%s=v.buffer.scale%s.%s' % tuple([a, b] * 2))

    for a in ['X', 'Y', 'I']:
        for b in ['description', 'unit']:
            exec('buff.scale%s.%s=v.buffer.scale%s.%s' % tuple([a, b] * 2))

    return buff


def resize(im, buff=None, mask_zero=[], force_new=True, force_recalc=False,
           name='', root='', append_name=True, **kwargs):
    """
    Resize im to buff (every frame).

    Parameters
    ----------
    im: IM subclass (IM7 or VC7)
    buff: BufferType | BufferTypeAlt
        Target buffer (not including frames)
    mask_zero: list
        send im to set_zero prior to resizing. See function "IM.filter.set_zero"
    force_new: Bool
        A new object is made, the name will be incremented if an object exists
        with the same name and root.
    force_recalc: Bool
        Will redo even if target object already loaded.
    name: str
        Name for the new object.
    root may be passed as a named argument.
    **kwargs correspond to scipy.ndimage.map_coordinates
        default values are otherwise
        order       = 3
        prefilter   = True
        mode        = nearest

    Returns
    -------
    im: IM7 | VC7

    """

    if not name:
        name, root = im.get_new_name('resize', force_new, append_name,
                                     root=root)

    buff = ReadIM.BufferTypeAlt(buff)
    buff.nf = im.buffer.nf
    buff.image_sub_type = im.buffer.image_sub_type

    if 'family' in kwargs:
        family = kwargs.pop('family')
    else:
        family = str(im.family)

    try:
        if force_recalc or force_new:
            raise IOError
        return im.__class__(name, root=root, parents=im, family=family)
    except IOError:
        im_new = im.__class__(name, buff=buff, root=root, parents=im,
                              family=family)

    # transfer any variables
    im_new.data['store'].update(im.data['store'])
    im_new.variables.__dict__.update(im_new.data['store'])

    im_z = set_zero(im, mask_zero)

    image_args = {'order': 3,
                  'mode': 'nearest',
                  'prefilter': True}
    image_args.update(kwargs)

    newx = im_new.Px
    newy = im_new.Py
    x0 = im.buffer.scaleX.offset
    y0 = im.buffer.scaleY.offset
    dx = im.dx
    dy = im.dy
    jvals = (newx - x0) / dx
    ivals = (newy - y0) / dy * np.sign(im.buffer.scaleY.factor)

    for v, vn in zip(im_z.axes, im_new.axes):
        for f in range(im.frames):
            coords = np.array([ivals, jvals])
            mask = ndimage.map_coordinates(np.array(v[f].mask, dtype=float),
                                           coords, order=1, mode='constant',
                                           cval=1, prefilter=False)

            coords[:, (mask > 0)] = coords.max() * 10
            try:
                vf = np.array(v[f])
                vnf = ndimage.map_coordinates(vf, coords, **image_args)
            except BaseException:
                raise
            r = mask == 0
            vn[f][r] = vnf[r]
            vn[f].mask[r] = False

    im_new.notes = 'resized using resize'
    return im_new


def resize_bin(im, bin_fac, force_new=True, force_recalc=False,
               name='', root='', append_name=True, **kwargs):
    """
    Resize im to by binning.

    Parameters
    ----------
    im: IM subclass (IM7 or VC7)
    buff: BufferType | BufferTypeAlt
        Target buffer (not including frames)
    mask_zero: list
        send im to set_zero prior to resizing. See function "IM.filter.set_zero"
    force_new: Bool
        A new object is made, the name will be incremented if an object exists
        with the same name and root.
    force_recalc: Bool
        Will redo even if target object already loaded.
    name: str
        Name for the new object.
    root may be passed as a named argument.
    **kwargs correspond to scipy.ndimage.map_coordinates
        default values are otherwise
        order       = 3
        prefilter   = True
        mode        = nearest

    Returns
    -------
    im: IM7 | VC7

    """

    if not name:
        name, root = im.get_new_name('resize', force_new, append_name,
                                     root=root)

    buff = ReadIM.BufferTypeAlt(im.buffer)
    buff.nf = im.buffer.nf
    buff.nx = int(buff.nx / bin_fac)
    buff.ny = int(buff.ny / bin_fac)

    # u
    bin_facX = float(im.nx) / buff.nx
    bin_facY = float(im.ny) / buff.ny
    buff.scaleX.factor *= bin_facX
    buff.scaleY.factor *= bin_facY
##    buff.scaleX.offset = im.buffer.scaleX.offset - im.dx/2 + bin_facX * im.dx / 2
##    buff.scaleY.offset = im.buffer.scaleY.offset - im.dy/2 + bin_facY * im.dx / 2

    buff.totalLines = buff.nf * buff.ny * buff.nz
    buff.image_sub_type = im.buffer.image_sub_type

    family = kwargs.get('family')
    if not family:
        family = im.family

    try:
        if force_recalc or force_new:
            raise IOError
        return im.__class__(name, root=root, parents=im, family=family)
    except IOError:
        im_new = im.__class__(name, buff=buff, root=root, parents=im,
                              family=family)

    # transfer any variables
    im_new.data['store'].update(im.data['store'])
    im_new.variables.__dict__.update(im_new.data['store'])

    for v, vn in zip(im.axes, im_new.axes):
        for f in range(im.frames):
            vn[f] = strided_rescale(v[f], bin_fac)
            vn[f].mask = strided_rescale(v[f].mask, bin_fac)

    # adjust camera pixel size accordingly
    atts = {}
    atts.update(im.attributes)
    for att in atts:
        if att.find('CamPixelSize') == 0:
            rec = atts[att]
            val, units = rec.split(' ')
            atts[att] = '{0:0.1f} {1}'.format(float(val) * bin_fac, units)

    im_new.attributes.update(atts)
    im_new.notes = 'resized using resize_bin'
    return im_new


from numpy.lib.stride_tricks import as_strided


def strided_rescale(g, bin_fac):
    strided = as_strided(g,
                         shape=(g.shape[0] // bin_fac, g.shape[1] //
                                bin_fac, bin_fac, bin_fac),
                         strides=((g.strides[0] * bin_fac, g.strides[1] * bin_fac) + g.strides), writeable=False)
    return strided.mean(axis=-1).mean(axis=-1)


def interpFrames(im, frames, force_new=True, force_recalc=False, order=3,
                 mode='constant', cval=0.0, prefilter=True, name='', root='',
                 append_name=True, **kwargs):
    """
    Interpolate between frames using scipyt.ndimage.map_coordinates.
    Extrapolation not performed, data outside will be masked.

    Parameters
    ----------
    im: IM7 | VC7
        Input image or vector with multiple frames.
    frames : list, array
        Values between frames of which to return.
        eg. frames [0.5, 1.5, 2.5] will return an im obj with halfway between
            the first, second and third frames.
    force_new: Bool
        A new object is made, the name will be incremented if an object exists
        with the same name and root.
    force_recalc: Bool
        Will redo even if target object already loaded.
    order: int, optional
        The order of the spline interpolation, default is 3.
        The order has to be in the range 0-5.
    mode: str, optional
        Points outside the boundaries of the input are filled according
        to the given mode ('constant', 'nearest', 'reflect' or 'wrap').
        Default is 'constant'.
    cval: scalar, optional
        Value used for points outside the boundaries of the input if
        ``mode='constant'``. Default is 0.0
    prefilter: bool, optional
        The parameter prefilter determines if the input is pre-filtered with
        `spline_filter` before interpolation (necessary for spline
        interpolation of order > 1).  If False, it is assumed that the input is
        already filtered. Default is True.

    Returns
    -------
    im: IM7 | VC7
        Interpolated version of input im.

    Notes
    -----
    Frames of interpolated object are stored as a variable inside the returned
    object. as im.variables.frames. Returned image is a child of the input object.

    """
    name = kwargs.get('name')
    root = kwargs.get('root', im.root)

    if not name:
        name, root = im.get_new_name(
            'interpFrames', force_new, append_name, root=root)

    family = kwargs.get('family')
    if not family:
        family = im.family

    try:
        if force_recalc or force_new:
            raise IOError
        return im.__class__(name, root=root, parents=im, family=family)

    except IOError:

        buff = ReadIM.BufferTypeAlt(im.buffer)
        buff.nf = len(frames)
        im_new = im.__class__(name, buff=buff, root=root, parents=im,
                              family=family)

    newx = im_new.Px
    newy = im_new.Py
    x0 = im.buffer.scaleX.offset
    y0 = im.buffer.scaleY.offset
    dx = im.dx
    dy = im.dy
    jvals = (newx - x0) / dx
    ivals = -(newy - y0) / dy

    zvals = np.ones_like(jvals)

    for v, vn in zip(im.axes, im_new.axes):

        for i, f in enumerate(frames):
            coords = np.array([zvals * f, ivals, jvals])
            mask = ndimage.map_coordinates(np.array(v.mask, dtype=float),
                                           coords, order=1, mode='constant',
                                           cval=1, prefilter=False)

            vnf = ndimage.map_coordinates(v, coords, order=order, mode=mode,
                                          cval=cval, prefilter=prefilter)
            vn[i] = vnf

            r = mask == 1
            vn.mask[i][r] = True

    im_new.variables.store(frames=frames)
    im_new.notes = 'interpolated "{0}" using interpFrames'.format(im)
    return im_new


def resampleFrames(im, M=1, force_new=False, force_recalc=False,
                   order=3, mode='constant', cval=0.0, prefilter=True,
                   name='', root='', append_name=True, **kwargs):
    """
Resample image/vector frames by zooming between frames using "ndimage.zoom".
******************    WARNING   *******************************
Less accurate than interpFrames.

******************    WARNING   *******************************

    Parameters
    ----------
    M   : float
    factor to zoom. eg. M=2 will double the number of frames
    M will be adjusted to M = floor(frames/M) + 1, which will line up better
    with existing images.

    input : ndarray
        The input array.
    zoom : float or sequence, optional
        The zoom factor along the axes. If a float, `zoom` is the same for each
        axis. If a sequence, `zoom` should contain one value for each axis.
    output : ndarray or dtype, optional
        The array in which to place the output, or the dtype of the returned
        array.
    order : int, optional
        The order of the spline interpolation, default is 3.
        The order has to be in the range 0-5.
    mode : str, optional
        Points outside the boundaries of the input are filled according
        to the given mode ('constant', 'nearest', 'reflect' or 'wrap').
        Default is 'constant'.
    cval : scalar, optional
        Value used for points outside the boundaries of the input if
        ``mode='constant'``. Default is 0.0
    prefilter : bool, optional
        The parameter prefilter determines if the input is pre-filtered with
        `spline_filter` before interpolation (necessary for spline
        interpolation of order > 1).  If False, it is assumed that the input is
        already filtered. Default is True.
    Returns
    -------
    im: IM7 | VC7


    """

    name = kwargs.get('name')
    root = kwargs.get('root', im.root)

    # calculate the number of frames that will be produced
    frames = int(max(np.floor(im.frames * M), 1))
    M = float(frames) / im.frames

    if not name:
        name, root = im.get_new_name('interp_M={0}'.format(M),
                                     force_new, kwargs.get('append_name', True), root=root)

    # build the new object
    buff = ReadIM.BufferTypeAlt(im.buffer)
    buff.nf = frames

    family = kwargs.get('family')
    if not family:
        family = im.family

    try:
        if force_recalc or force_new:
            raise IOError
        return im.__class__(name, root=root, parents=im, family=family)
    except IOError:
        im_new = im.__class__(name, buff=buff,
                              root=root, parents=im, family=family)

    # perform the operation

    for v, vn in zip(im.axes, im_new.axes):
        vn[:] = ndimage.zoom(v, (M, 1, 1))
        mask = ndimage.zoom(v.mask, (M, 1, 1), order=order, mode='constant',
                            cval=1, prefilter=False)
        vn.mask[:] = mask > 0.5

    if 'phase_f' in im.variables:
        phase_f = ndimage.zoom(v.mask, (M, 1, 1),
                               order=order, mode=mode, cval=cval,
                               prefilter=prefilter)

    indexes = np.linspace(0, im.frames - 1, im_new.frames)
    im_new.variables.store(frames=indexes)
    im_new.notes = 'resampled "{0}" using resampleFrames'.format(im)

    return im_new


def set_zero(im, mask_zero=[], force_new=True, name='', root='',
             append_name=True):
    """
    Returns a copy of im with the pixels falling inside mask_zero set to zero.
    where mask_zero is a list of keys corresponding to the coordinates in the
    dictionary at im.mask_coordinates (details below). Coordinates are generated
    using the method IM.make_mask.

    Parameters
    ----------
    im: IM7 | VC7
        Object to modify.

    mask_zero: list of keys in im.mask_coordinates. A dictionary of lists of
        coordinates to mark a region.
            Example of an entry in im.mask_coordinates:
                mask_coords['border']=(         (65,-420),
                                                (-113,-360),
                                                (-260,-210),
                                                (-280,-140),
                                                (-280,-420),
                                                (65,-420))
            new coordinates can be added to im.mask_coordinates using
            standard dictionary methods.

    Returns
    -------
    im: IM7 | VC7
        copy of input

    """
    im_new = im.copy(name, force_new, root, append_name)
    set_zero = np.ones((im.ny, im.nx), dtype=bool)

    for n in mask_zero:
        if n not in im.mask_coordinates:
            raise IndexError('mask is not loaded: %s' % n)
        set_zero = set_zero & im.make_mask(n)
    set_zero = np.invert(set_zero)  # flip for logic

    # set the value and mask
    for a in im_new.axes:
        a.data[:, set_zero] = 0

    im_new.notes = 'applied mask:{0}'.format(mask_zero)
    return im_new


def create_group(name, filenames, buff=None, force_reload=False,
                 mode='set_zero', ims=[], mask_zero=[], root=None, family='', resizeArgs={}):
    """
    Combine all ims into a single buffer object frame by frame in order
    of filenames. If an im has more than one frame all frames are appended
    by im.

    Parameters
    ----------
    name: str
        Name of the returned object

    filenames: list
        List of the filenames or ims of the objects to be combined.

    buff: BufferType | BufferTypeAlt
        Target buffer for the returned object, a copy of the buffer is used
        if it is provided

    force_reload: Bool
        If the targed object from root / name exists as a file.
            The target file will be deleted before processing.

    mode: str
         corresponds to a function for resizing ['resize', 'set_zero']

    mask_zero: list
        list of keys corresponding to IM.IM.mask_coordinates. See also set_zero.

    family: str
        family name is required if images are from multiple families.

    Returns
    -------
    im: IM7 | VC7

    """
    if not force_reload:
        try:
            im = IMloader(filenames[0], family=family)
            im = im.__class__(name, root=root, family=family)
            return im
        except BaseException:
            pass

    types = set()
    loadfiles = []
    if not ims:
        ims = []  # make a new list (keep in memory)
        print('Loading files (%s)' % len(filenames))
        for f in filenames:
            im = IMloader(f, family=family)
            types.add(im._type)
            ims.append(im)
            if len(types) > 1:
                raise TypeError('Can only group one type at a time%s' % types)
            for i in range(im.frames):
                loadfiles.append(im.loadfile)

    if not family:
        if len(set([i.family for i in ims])) > 1:
            mesg = 'Merging objects from multiple familys. '\
                'Please specify a family to asign to in kwargs'
            raise RuntimeError(mesg)
        family = ims[0].family

    if buff is None:
        buff = make_common_buffer(ims)

    buff = ReadIM.BufferTypeAlt(buff)

    buff.nf = sum(im.frames for im in ims)

    if ims:
        im = ims[0]
    else:
        im = IMloader(filenames[0], family=family)
    if not force_reload:
        try:
            im = im.__class__(name, root=root, family=family)
            if not buff.nf == im.frames:
                raise AttributeError(
                    'File exists already. But miss-match in number of frames.'
                    ' Use "force_reload=True to override.')
            return im
        except IOError:
            pass

    im_new = im.__class__(name, buff=buff, root=root, family=family)
    if os.path.isfile(str(im_new)):
        os.remove(str(im_new))
        im_new = im.__class__(name, buff=buff, root=root, force_reload=True,
                              family=family)

    # loop image by image
    i = 0
    for (f, im) in zip(filenames, ims):

        # check arrays are the right shape otherwise resize them
        # resizing is used to mask out the zero pixels
        if (im.window != im_new.window) or (
                im.shape != im_new.shape) or mask_zero:
            if mode == 'resize':
                import warnings
                mesg = 'vs have been resized!\n consider the masking region'\
                    ' carefully as data is interpolated automatically'
                warnings.warn(mesg)
                im = resize(im, im_new.buffer, mask_zero, True, **resizeArgs)
            if mode == 'set_zero':
                im = set_zero(im, mask_zero, True)

    # copy each axes
        assert im_new.shape[1:] == im_new.shape[1:]
        sl = slice(i, i + im.frames)
        for a, b in zip(im_new.axes, im.axes):
            a[sl, :] = b
        i += im.frames
    if loadfiles:
        im_new.loadfiles = loadfiles
    return im_new


def rep_seq(l, sep='_', red_symb='-'):
    """ Represent a sequence in a shortened format.
    Used by get_merge_name. Convenient to override process by replacing this
    function.
    """
    out = []
    ll = sorted(set([str2num(s, '', '') for s in l]))
    if __SHORTEN_NUM_SEQ__ and all([isinstance(s, int) for s in ll]):
        for k, g in itertools.groupby(
                enumerate(ll), lambda i_x: i_x[0] - i_x[1]):
            sl = list(map(itemgetter(1), g))
            if len(sl) <= 2:
                for a in sl:
                    out.append('{0}'.format(a))
            else:
                out.append('{start}{red_symb}{finish}'.format(start=sl[0],
                                                              finish=sl[-1],
                                                              red_symb=red_symb,
                                                              ))
    else:
        out = [str(s) for s in ll]

    return sep.join(out)


def get_merge_name(pths=[], name='', shorten=True, subfoldersort=False):
    """
    Combine the pths in a controlled method such that the order of the paths in
    pths is not important producing a repeatable name.
    pths: list
        List of relative or absolute path names.

    name: str
        If provided the name joined as a subfile.

    shorten: bool
        Takes the last parameter set "_[something]=[value]"

    subfoldersort: bool


     = logical name from relative paths, where relative paths are the relative
    paths of all vs for renaming
    shorten = True causes non-identical levels to extract the symbol
              implying the level should be one character long

    Returns
    -------
    merged_name: string
        The merged name, if name is provided then name is appended after
            merged_name with and underscore separator.

    """

    def combine(l):
        """ Combine a list of paths
        """
        overlaps = []
        # in the event of numbers this is simple to join them.
        if l and all([isinstance(c, STR_TYPE)
                      for c in l]) and all([c.isdigit() for c in l]):
            # sort assending and join
            return "_".join([str(c) for c in sorted([int(c) for c in l])])
        else:
            overlaps = list(zip(*[a for a in l if a]))
        cl = []

# sort at sub levels/folders
        for o in overlaps:
            o = set(o)

            # special case for numbers
            if subfoldersort:  # check all can be converted to a number
                o = sorted([str2num(s, sep='', sepdict='') for s in o])
                # try and reduce if sequences of integers exists
                o = rep_seq(o, '_', '-')
                cl.append(o)
            else:
                cl.extend(sorted(o))
        cl = '_'.join(cl)
        return cl

    relative_paths = []
    drive = ''
    for p in pths:
        drive, rp = os.path.splitdrive(p)
        relative_paths.append(rp)

    relative_paths = [p.split(os.sep) for p in relative_paths]
    paths = [set(r) for r in zip(*relative_paths)]
    common = set()
    name_parts = []

    fname = ''
    if drive:
        fname = os.sep.join([drive, ''])  # add the drive if an absolute path

    for p in paths:
        if len(p) == 1:
            fname = os.path.join(fname, p.pop())
        else:
            if shorten:  # Shorten assumes that each letter can only be use once and is alphabetical

                try:
                    # look at named attributes
                    prefix = []
                    suffix = []
                    name_atts = []
                    values = {}
                    for a in p:
                        atts = get_name_atts(a, False)
                        assert atts

                        # see if there is any prefix info (do suffix later)
                        # last _ before the first =
                        i = a.rfind('_', 1, a.find('='))
# prefix.append(a[:i].split('_'))
                        prefix.append(a[:i])

                        for key in list(atts.keys()):
                            if key not in name_atts:
                                name_atts.append(key)
                                values[key] = set()
                            values[key].add(atts[key])

                # look for a suffix
                        if i < len(a):
                            # try first if there are named attributes
                            ir = a.find('_', a.rfind('='))
                            if not name_atts and ir < i:
                                ir = i
                        if ir > 0:
                            suffix.append(a[ir:].strip('_').split('_'))

                    prefix = combine(prefix)
                    suffix = combine(suffix)

                    shortname = '_'.join(
                        ['%s={%s}' % (n, n) for n in name_atts])
                    for k in values:
                        values[k] = '-'.join([a for a in values[k]])
                    components = [a for a in [prefix, shortname.format(
                        **values), suffix] if a]  # strip any missing info
                    assert components
                    shortname = '_'.join(components)

                except AssertionError:

                    shortname = '_'.join([rep_seq(a, '_', '-')
                                          for a in zip(*[a.split('_') for a in p])])
##                    shortname = '_'.join(['_'.join(sorted(set(a))) for a in zip(*[a.split('_') for a in p])])

                except BaseException:
                    raise
                    # work with single letters (robust)
                    shortname = ''.join(sorted(set(''.join(p))))

                fname = os.path.join(fname, shortname)

            else:
                fname = os.path.join(fname, ''.join(sorted(p)))

    if name:
        fname = '_'.join([fname, name])
    return fname


def merge_wts(ims, buff=None, mask_zero=[], force_recalc=False, w_fac=1, name='',
              root='', family='', resultOBJ=None, **kwargs):
    """
    Creates a mosaic of the ims using a weighted average method. Images are resized
    to fit the full window or buff. [see: IM.filter.resize]
    Assumes each frame of the vectors should be merged to the final image therefore
    all im must have the same number of frames.

    Parameters
    ----------
    ims: list [IM7 | VC7]
        List of images or vectors. All items must have the same number of axes
        and frames.

    buff: BufferType | BufferTypeAlt
        Optional target buffer to fit into. The default buffer will spatially
        fit the smallest window to cover the windows of all images and use the
        dx and dy from the first. See IM.filter.make_common_buffer for further
        details.

    mask_zero: list
        list of keys corresponding to IM.IM.mask_coordinates.
        See: IM.filter.set_zero for further details.

    force_recalc: Bool
        Will redo even if target object already loaded. If the target object
        has a file then it will be deleted otherwise it will be returned without
        calculation.

    w_fact: float
        Weighting factor which diminishes the impact of the edge of the images
        at overlap. which uses the w = IM.filter.blur_image(im, n, ny) function
        to produce the weightings. im is an array of ones the same shape as the
        im to be merged.

    name: str
        Name for the new object.

    family: str
        family name is required if images are from multiple families.

    resultOBJ: None
        object class to put result

    **kwargs: dict
         arguments are passed to IM.filter.resize
         defaults = {'order':3,
                     'mode':'nearest',
                     'prefilter':True}

    Returns
    -------
    im: IM7 | VC7

    """

    if name:
        merged_name = name
    else:
        # find out what is in common and what is different
        relative_paths = [v.name for v in ims]
        merged_name = get_merge_name(relative_paths, 'merge=wts',
                                     subfoldersort=True)

    if not family:
        assert len(set([i.family for i in ims])) == 1, 'Merging objects from'\
            ' multiple familys. Please specify a family to asign to in kwargs'

        family = ims[0].family

    try:
        # check if the file already exists
        im = ims[0].__class__(merged_name, root=root, family=family)
        if force_recalc:
            # remove the existing file
            os.remove(str(im))
            del(im)
            raise IOError
        else:
            return im

    except IOError:
        if buff is None:
            buff = make_common_buffer(ims)

        buff = ReadIM.BufferTypeAlt(buff)
        buff.nf = ims[0].frames
        buff.image_sub_type = ims[0].buffer.image_sub_type
        resultOBJ = resultOBJ or ims[0].__class__
        merged = resultOBJ(merged_name, root=root, buff=buff,
                           parents=ims,
                           family=family)

    # resize
    print('resizing ims, this could take a while')

    if len(set([v.frames for v in ims])) != 1:
        raise IndexError('ims cannot have a different number of frames!')

    # process on a frame by frame process to keep memory usage down
    for frame in range(merged.frames):
        # print "{0} members in family".format(len(ims[0].members_family))
        rs = [resize(v[frame], buff, mask_zero, True, **kwargs) for v in ims]

        def get_coords(merged, im):
            """ coordinates for image mapping/resizing
            """
            newx = merged.Px
            newy = merged.Py
            x0 = im.buffer.scaleX.offset
            y0 = im.buffer.scaleY.offset
            dx = im.dx
            dy = im.dy
            jvals = (newx - x0) / dx
            ivals = -(newy - y0) / dy
            coords = np.array([ivals, jvals])
            return coords

        wts = []

        for v in ims:
            v = v[frame]
            # calculate weightings
            w = blur_image(np.ones((v.shape[1], v.shape[2])), max(
                20, w_fac * v.ny), max(20, w_fac * v.nx))

            # resize
            coords = get_coords(merged, v)
            w = ndimage.map_coordinates(
                w, coords, order=1, cval=0, prefilter=False)
            wts.append(w)

        # weights to diminish significance at the overlaps
        wts = np.array(wts)

        # get all intensities for all axes (have to do this first)
        II = []
        for i in range(len(ims[0].axes)):
            II.append(np.ma.array([im.axes[i] for im in rs]))

        # put data into merged object
        for a, b in zip(merged.axes, II):
            av = np.ma.average(b[:, 0], 0, wts)
            a[frame] = av

    if hasattr(ims[0].variables, 'phase_f'):
        phase_f = getattr(ims[0].variables, 'phase_f', None)
        merged.store_variable(('phase_f', phase_f))

    ims_names = ', '.join([im.name for im in ims])
    merged.notes = 'merge=wts\nims=[{0}]\nw_fac={0}s'.format(ims_names, w_fac)

    return merged


def filter(im, func, force_new=True, extend_mask=False, name='', root='',
           append_name=True, **kwargs):
    """
    Return a filtered copy of im.

    Parameters
    ----------
    im: IM7 | VC7
        Filter is applied to all frames.

    func: str | callable
        Name of the function in scipy.ndimage.filter[func_name]. Or is a
        callable function of the form: func(input,[args]). Args are passed
        via this function call vi named arguments (**kwargs). Must accept
        the named arguments input and output which should be arrays of the same
        shape.

    extend_mask: bool
         The mask is extended based on the affect of the filter, usually over
         conservative.

    COPIED VERBATIM FROM Scipy.ndimage.generic_filter
    Calculates a multi-dimensional filter using the given function.

    At each element the provided function is called. The input values
    within the filter footprint at that element are passed to the function
    as a 1D array of double values.

    Parameters
    ----------
    input : array-like
        input array to filter
    function : callable
        function to apply at each element
    size : scalar or tuple, optional
        See footprint, below
    footprint : array, optional
        Either ``size`` or ``footprint`` must be defined.  ``size`` gives
        the shape that is taken from the input array, at every element
        position, to define the input to the filter function.
        ``footprint`` is a boolean array that specifies (implicitly) a
        shape, but also which of the elements within this shape will get
        passed to the filter function.  Thus ``size=(n,m)`` is equivalent
        to ``footprint=np.ones((n,m))``.  We adjust ``size`` to the number
        of dimensions of the input array, so that, if the input array is
        shape (10,10,10), and ``size`` is 2, then the actual size used is
        (2,2,2).
    output : array, optional
        The ``output`` parameter passes an array in which to store the
        filter output.
    mode : {'reflect','constant','nearest','mirror', 'wrap'}, optional
        The ``mode`` parameter determines how the array borders are
        handled, where ``cval`` is the value when mode is equal to
        'constant'. Default is 'reflect'
    cval : scalar, optional
        Value to fill past edges of input if ``mode`` is 'constant'. Default
        is 0.0
    origin : scalar, optional
    The ``origin`` parameter controls the placement of the filter. Default 0
    extra_arguments : sequence, optional
        Sequence of extra positional arguments to pass to passed function
    extra_keywords : dict, optional
        dict of extra keyword arguments to pass to passed function
    """
    if isinstance(func, collections.Callable):
        func_name = func.__name__
    else:
        func_name = func
        func = getattr(ndimage, func)

    if not name:
        name = '_'.join([func_name, '_'.join(['%s=%s' % (k, v)
                                              for k, v in sorted(kwargs.items())])])

    new_im = im.copy(name, force_new, root, append_name)

    for a, b in zip(new_im.axes, im.axes):

        # ,*extra_arguments,**extra_keywords)
        func(input=b, output=a, **kwargs)

        # the mask is at least as big as the original but also
        # isolate any nan data that slipped through
        mask = b.mask | np.isnan(a)

        if extend_mask:
            mask = mask | (
                func(input=np.array(b.mask, dtype=float), **kwargs) > 0)
        a.mask[:] = b.mask
    return new_im


def resize_to_window(im, window, force_new=True, force_recalc=False,
                     name='', root='', append_name=True, bin_fac=1, **kwargs):
    """
    Resize im to window of closest fit. No interpolation or smoothing is done.

    Parameters
    ----------
    im: IM subclass (IM7 or VC7)
    buff: BufferType | BufferTypeAlt
        Target buffer (not including frames)
    mask_zero: list
        send im to set_zero prior to resizing. See function "IM.filter.set_zero"
    force_new: Bool
        A new object is made, the name will be incremented if an object exists
        with the same name and root.
    force_recalc: Bool
        Will redo even if target object already loaded.
    name: str
        Name for the new object.
    root may be passed as a named argument.


    Returns
    -------
    im: IM7 | VC7

    """

    if not name:
        name, root = im.get_new_name('resize_window', force_new, append_name,
                                     root=root)

    buff = ReadIM.BufferTypeAlt(im.buffer)

    px1, py1 = im.map_XY(window[0][0], window[0][1])
    px2, py2 = im.map_XY(window[1][0], window[1][1])

    px1 = int(max(round(px1), 0))
    py1 = int(max(round(py1), 0))
    px2 = int(min(round(px2), im.buffer.nx))
    py2 = int(min(round(py2), im.buffer.ny))

    nx, ny = im.axes[0][0][py1:py2:bin_fac, px1:px2:bin_fac].shape

    buff.nx = ny
    buff.ny = nx

    nx_new, ny_new = im.axes[0][0][py1:py2:1, px1:px2:1].shape

    # u
    bin_facX = float(nx_new) / nx
    bin_facY = float(ny_new) / ny
    buff.scaleX.factor *= bin_facX
    buff.scaleY.factor *= bin_facY

    x1, y1 = im.map_xy(px1, py1)

    buff.scaleX.offset = x1
    buff.scaleY.offset = y1

##    buff.scaleX.factor *= bin_fac
##    buff.scaleY.factor *= bin_fac

    family = kwargs.get('family')
    if not family:
        family = im.family

    try:
        if force_recalc or force_new:
            raise IOError
        return im.__class__(name, root=root, parents=im, family=family)

    except IOError:
        im_new = im.__class__(name, buff=buff, root=root, parents=im,
                              family=family)

    # transfer any variables
    im_new.data['store'].update(im.data['store'])
    im_new.variables.__dict__.update(im_new.data['store'])

    for v, vn in zip(im.axes, im_new.axes):
        for f in range(im.frames):
            vn[f] = v[f][py1:py2:bin_fac, px1:px2:bin_fac]
            vn[f].mask = v[f].mask[py1:py2:bin_fac, px1:px2:bin_fac]

    # adjust camera pixel size accordingly
    atts = {}
    atts.update(im.attributes)
    for att in atts:
        if att.find('CamPixelSize') == 0:
            rec = atts[att]
            val, units = rec.split(' ')
            atts[att] = '{0:0.1f} {1}'.format(float(val) * bin_fac, units)

    im_new.attributes.update(atts)
    im_new.notes = 'resized using resize_to_window'
    return im_new


def filter_median(im, size=3, force_new=True, **kwargs):
    """
    Median filter see IM.filter.filter for further details and
            scipy.ndimage.median_filter

    Parameters
    ----------
    im: IM7 | VC7
    size:

    Returns
    -------
    im: IM7 | VC7
    """

    return filter(im, 'median_filter', force_new, size=size, **kwargs)


def filter_gaussian(im, sigma=1, force_new=True, **kwargs):
    """
    sigma = standard deviation
    """
    return filter(im, 'gaussian_filter', force_new, sigma=sigma, **kwargs)


def filter_gaussian_blur(im, n, force_new=True):
    """blur
    """

    new_im = im.copy('gaussian_filter', force_new)

    for a, b in zip(new_im.axes, im.axes):
        for frame in range(new_im.frames):
            a[frame] = blur_image(b[frame], n)

    new_im.notes = 'applied gausian filter to: %s\nn = %0.2f' % (im.name, n)
    return new_im


def overlaps_count(ims, buff=None, nx=1, ny=1):
    """
    Count the number of overlapping windows between all ims.

    Parameters
    ----------
    ims: list [IM7 | VC7]
        list of images or vectors.

    Returns
    -------
    count: int
    """

    family = ims[0].family

    if buff is None:
        buff = make_common_buffer(ims)
        buff.nf = 1

    imt = ims[0].__class__('Total buff for splicing', buff=buff, family=family)

    counter = 0
    for a, b in itertools.combinations(ims, 2):

        # get a list of the pixels which match the overlap
        aPx, aPy = imt.map_XY(*a.map_xy(*np.ogrid[0:a.nx, 0:a.ny]))
        bPx, bPy = imt.map_XY(*b.map_xy(*np.ogrid[0:b.nx, 0:b.ny]))
    # get the overlapping pixels
        cPx = np.lib.arraysetops.intersect1d(np.array(aPx, dtype=int),
                                             np.array(bPx, dtype=int))
        cPy = np.lib.arraysetops.intersect1d(np.array(aPy, dtype=int),
                                             np.array(bPy, dtype=int))

        if (cPx.size >= nx) & (cPy.size >= ny):
            counter += 1

    return counter


def overlaps_splice_iter(ims, buff=None, nx=1, ny=1, force_recalc=False,
                         family=''):
    """
    A generator which yields a phase sorted object of the same type as the input
    consisting of all objects whose windows overlap.

    Parameters
    ----------
    ims: list [VC7 | IM7]
        A list of objects with the same number of frames and overlapping windows.
        each im in ims must have the variable ims.variables.phase_f. A 1d array
        of length matching the frames.
    buff: BufferType | BufferTypeAlt
        The buffer with parameters that will be assigned to the generated ims.
    nx, ny: int
        The minimum overlap to be considered.

    return a list containing all overlapping combinations (2x only) for
    phase sorted data. imput ims must have the variable phase_f
    data is automatically copied to the overlapping data

    Yields
    ------
    im: IM7|VC7

    """

    if not family:
        assert len(set([i.family for i in ims])) == 1, 'Merging objects from'\
            ' multiple familys. Please specify a family to asign to in kwargs'

        family = ims[0].family

    if buff is None:
        buff = make_common_buffer(ims)

    buff2 = ReadIM.BufferTypeAlt(buff)
    buff2.nf = 1  # only using this buffer for mapping positions
    imt = ims[0].__class__('Total buff for splicing',
                           buff=buff2, family=family)

    newims = []  # storage for the return object
    for a, b in itertools.combinations(ims, 2):

        # get a list of the pixels which match the overlap
        aPx, aPy = imt.map_XY(*a.map_xy(*np.ogrid[0:a.nx, 0:a.ny]))
        bPx, bPy = imt.map_XY(*b.map_xy(*np.ogrid[0:b.nx, 0:b.ny]))
    # get the overlapping pixels
        cPx = np.lib.arraysetops.intersect1d(np.array(aPx, dtype=int),
                                             np.array(bPx, dtype=int))
        cPy = np.lib.arraysetops.intersect1d(np.array(aPy, dtype=int),
                                             np.array(bPy, dtype=int))

        if (cPx.size >= nx) & (cPy.size >= ny):

            # store the merged result in a new image/vector
            overlap_window = (
                (imt.map_xy(cPx[0], cPy[0])), imt.map_xy(cPx[-1], cPy[-1]))

            name = get_merge_name([a.name, b.name])
            buff = ReadIM.newBuffer(overlap_window, len(cPx), len(cPy),
                                    imt.buffer.vectorGrid,
                                    a.buffer.image_sub_type,
                                    a.frames + b.frames)

            try:
                if force_recalc:
                    raise IOError
                yield ims[0].__class__(name, family=family)
                continue
            except BaseException:
                newim = ims[0].__class__(name, buff=buff, family=family)

            # interpolate as required and shift the pixels
            a = resize(a, buff=buff, force_new=True)
            b = resize(b, buff=buff, force_new=True)

            # copy data to the new frame and sort it
            phase_f = np.hstack([a.variables.phase_f, b.variables.phase_f])
            r = np.argsort(phase_f)
            for cc, aa, bb in zip(newim.axes, a.axes, b.axes):
                cc[0:aa.shape[0]] = aa
                cc[0:aa.shape[0]].mask = aa.mask
                cc[aa.shape[0]:] = bb
                cc[aa.shape[0]:].mask = bb.mask
                cc[:] = cc[r]
            newim.store_variable(('phase_f', phase_f[r]))
            newim.store_variable(('r', r))
            newim.notes = 'created an overlap array using:\n%s\nand\n%s' % (
                a.name, b.name)
            yield newim


def gauss_kern(size, sizey=None):
    """ Returns a normalized 2D gauss kernel array for convolutions """
    size = int(size)
    if not sizey:
        sizey = size
    else:
        sizey = int(sizey)
    x, y = np.mgrid[-size:size + 1, -sizey:sizey + 1]
    g = np.exp(-(x**2 / float(size) + y**2 / float(sizey)))
    return g / g.sum()


def blur_image(im, n, ny=None):
    """
    Blurs the image by convolving with a normalized 2D gaussian kernel of typical
    size n. The optional keyword argument ny allows for a different
    size in the y direction.

    Parameters
    ----------
    im: array - at least 2D

    n: int
        Size in pixels of the Gaussian kernel.

    ny: int
        If provided the size in pixels of the vertical axis of the
        Gaussian kernel.

    Returns
    -------
    im:
    """
    g = gauss_kern(n, sizey=ny)
    improc = signal.convolve(im, g, mode='same')
    return improc
